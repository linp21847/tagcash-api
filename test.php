<?php
function httpget($url){
    if(is_callable('curl_init')) {
        $ch= curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result= curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result, true);
        return $json;
    } else {
        return false;
    }
}
function downloadfile( $url ){
    $ch = curl_init( $url );
    $filename = basename( $url );
    $fp = fopen( $filename , 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
    return $filename;
}

if($_GET['user_id'])
{
	$user_id = $_GET['user_id'];
}
else
{
	$user_id = '1493';
}

if($_GET['media_id'])
{
	$media_id = $_GET['media_id'];
}
else
{
	$media_id = '860';
}
//$user_id = '1486';

//$user_id = '167'; 
//$media_id = '899';

//$media_id = '2964';
$tooltip_array = array();
$out= httpget('http://front-api.tagcash.tv/sharing/media/'.$media_id.'/user/'.$user_id);

if ( $out ) {
    $tooltip_array  = $out['data'] ;
}

$index = 0;
foreach ($tooltip_array['hotspot_item'] as  $product) {
    foreach ($product['tagging_items'] as $hotspot ) {
        $index++;
        
    }   
}

//  echo "<pre>";print_r($out);echo "</pre>";
?>
<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <meta charset="utf-8">
        <title>TagCash</title>
        <link href="css/style.css?v=17" media="screen" rel="stylesheet" type="text/css" />
        <link href="css/responsive.css?v=1" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    </head>
    <body>
	
	<script>
 
		function Relocate(ele)
		{
			var x = $( "#"+ele ).position();

			//default
			$( "#bob_"+ele ).css('top','-52px');
			$( "#bob_"+ele ).css('left','-150px'); 
				
			var half_box = 164;
			var content_w = $('#pro1').width();
			 
			//left site
			var v_left = x.left ;
			if(half_box > x.left)
			{
				$( "#bob_"+ele ).css('left','-'+v_left+'px');
			}

			//right site
			var v_right = half_box - (content_w - half_box - x.left ) ; 
			var c_right = content_w  - x.left ; 
			if(half_box > c_right)
			{
				$( "#bob_"+ele ).css('left', '-'+v_right+'px');
			}

		}
			
		jQuery( document ).ready(function() {

			$('.peritem').click(function(){
				var item_id = $(this).attr('rel');
				var sectorParent = $(this).parent().parent().parent();
				sectorParent.find('.padd_box').hide();
				sectorParent.find('#' + item_id).fadeIn();
				sectorParent.find('.peritem').removeClass('active');
				$(this).addClass('active');
			});
		
			$( ".pro1 img.main_img" ).click(function(e) { 
				 alert(2);
				$( '.boxi_hov img.coin' ).css('visibility', function(i, visibility) {
					return (visibility == 'visible') ? 'hidden' : 'visible';
				});
			}).children().click(function(e) {
			  return false;
			});
			
			$( ".pro1" ).mouseenter(function() {
				
				$( '.boxi_hov img.coin' ).css("visibility", "visible");
				})
				.mouseleave(function() {
				$( '.boxi_hov img.coin' ).css("visibility", "hidden");
			});
			
			$( ".tp_box" ).mouseover(function() {
		 		 
				$( '.boxi_hov img.coin' ).css("visibility", "hidden");
				})
				.mouseout(function() {
				$( '.boxi_hov img.coin' ).css("visibility", "visible");
			});
				
			   <?php foreach ($tooltip_array['hotspot_item'] as  $hotspot) {  ?>
							Relocate("<?php echo $hotspot['id']; ?>");
				<?php } ?>
		})
	</script> 

        <div class="view_header">
            <div class="row">
                <div class="view_logo">
                    <a href="index.html">
                    <img src="images/201401/logo.png" alt="" />
                    </a>
                </div>
            </div>
            </div><!--header end-->

            <div id="content" class="pad_top">
                <div id="hotspot_page">

                    <div id="pro1" class="pro1"style="background-image: url(<?php echo $tooltip_array['thumb_url_orignal'];?>);">
                        <?php 
                      
                        foreach ($tooltip_array['hotspot_item'] as  $hotspot) {   

                                $coor_x1    = $hotspot['coor_x1']*100;
                                $coor_y1    = $hotspot['coor_y1']*100;
                        ?>
                        
                        <!--<div class="tool">-->
						        <div id="<?php echo $hotspot['id']; ?>" class="tol_im2" style="left:<?php echo $coor_x1; ?>%;top:<?php echo $coor_y1; ?>%;">
                                
								<div class="boxi_hov"  rel="<?php echo $hotspot['id'] ?>">
								<img class="coin" src="images/tag_icon.png"/>
								
								<div id="bob_<?php echo $hotspot['id'] ?>" class="tp_box_content">
                                <span class="tp_box tp_small">
								<?php $display="block"; ?>
								<?php $class="active"; ?>
								<?php foreach ($hotspot['tagging_items'] as $key => $tagelement) { ?>
								
									<div  class="padd_box" style="display:<?php echo $display; ?>">
								
										<div class="toltip_ima">
											<img src="<?php echo $tagelement['imageurl'] ?>"/>
										</div>

										<div class="item_info">
											<p class="item_store"><?php echo $tagelement['merchant'] ?></p>
											<p class="item_name"><?php echo $tagelement['name'] ?></p>
											<p class="item_price"><?php echo $tagelement['price'] ?> USD</p>
											
												<p class="item_btn_buy">
												<a target="_blank"  href="<?php echo $tagelement['link'] ?>" ></a>
													<!-- <a href"<?php echo $tagelement['link'] ?>"></a>-->
												</p> 
											
											<div class="clr"></div>
										</div>
										
									</div>
								
									<?php $display="none"; ?>
								<?php } ?>
 
                                </span>
									<div class="item_pag">
										<?php if(count($hotspot['tagging_items']) > 1) { ?>
										<?php $i = 1; ?>
										
											 
												<ul>
													<?php foreach ($hotspot['tagging_items'] as $key => $tagelement) { ?>
														<li id="page-item" class="peritem <?php echo $class;?>" rel="<?php echo $tagelement['id'] ?>"><?php echo $i ?></li>
														<?php $i++; ?>
														<?php $class=""; ?>
													<?php } ?>
												</ul>
											 

										<?php } ?>
									</div>

								
								<div class="clr"></div>
								</div> 
								</div> 
                            
							</div>
                       <!-- </div>-->
                       
                    <?php } ?>
                        <img class="main_img" src="<?php echo $tooltip_array['thumb_url_orignal'];?>" alt="tagcash user" >
                    </div>

                    <div id="media_info">
                        <div id="who_when_pic">
                            <img class="user_avatar" src="<?php echo $tooltip_array['thumb_avatar'];?>" alt="tagcash user">
                            <div id="who_when">
                                <div id="who"><?php echo $tooltip_array['name'];?></div>
                                <div id="when"><?php echo $tooltip_array['duration'];?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="title"><?php echo $tooltip_array['content'];?></div>
                        <div id="like_comm_tags">
                            <?php echo $tooltip_array['total_like'];?>&nbsp;Likes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $tooltip_array['total_comment'];?>&nbsp;Comment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo count($tooltip_array['hotspot_item']);?>&nbsp;Tags
                        </div>
                    </div>
                    </div><!--main part end-->

                    <div class="main_but">

                        <div id="downloadtheapp_call2action">
                            Download the App
                        </div>


                        <div id="gettheapp_buttons">
                            <a href="https://itunes.apple.com/us/app/tagcash/id718605786?mt=8" target="_blank" title="Download Tagcash on AppStore">
                            <div id="gettheapp_button_mac" class="gettheapp_button">
                            </div>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=tv.tagcash" target="_blank" title="Download Tagcash on GooglePlay">
                            <div id="gettheapp_button_android" class="gettheapp_button">
                            </div>
                            </a>
                        </div>

                    </div>


                </div>
            </body>
       </html>
        
        