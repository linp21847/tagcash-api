$(function () {

    $(".nav-toggle").click(function (e) {
        e.preventDefault();
        $(".top-nav-wrap").toggleClass('active');
        return false;
    });

    var windowWidth = $(window).width();

    // testimonials slider
    if ($('#bx-slider').length) {
        var slider = $('#bx-slider').bxSlider({
            captions: false,
            speed: 300,
            auto: false,
            controls: false,
            pagerCustom: '#custom_pager'
        });



        $('#slider-next').click(function(){
            slider.goToNextSlide();
            return false;
        });

        $('#slider-prev').click(function(){
            slider.goToPrevSlide();
            return false;
        });


        if (windowWidth > 840) {
            slider.reloadSlider({

                captions: false,
                speed: 800,
                auto: false,
                controls: false,
                mode: 'vertical',
                pagerCustom: '#custom_pager'
            });
        }

        $('.how-it-works-box').click(function () {
            var current = slider.getCurrentSlide();
            slider.goToNextSlide(current) + 1;
        });


    }
});
