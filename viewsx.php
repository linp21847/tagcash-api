<?php
function httpget($url){ 
    if(is_callable('curl_init')) {
        $ch= curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result= curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result, true);
        return $json;
    } else {
        return false;
    }
}
function downloadfile( $url ){
    $ch = curl_init( $url );
    $filename = basename( $url );
    $fp = fopen( $filename , 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
    return $filename;
}

if(isset($_GET['user_id']) && $_GET['user_id'])
{
	$user_id = $_GET['user_id'];
}
else
{
	$user_id = '1493';
}

if(isset($_GET['media_id']) && $_GET['media_id'])
{
	$media_id = $_GET['media_id'];
}
else
{
	$media_id = '860';
}
$tooltip_array = array();
$out= httpget('http://front-api.tagcash.tv/sharing/media/'.$media_id.'/user/'.$user_id);

if ( $out && isset($out['data']) ) {
    $tooltip_array  = $out['data'] ;
}


$index = 0;
if (isset($tooltip_array) && !empty($tooltip_array)) {
    foreach ($tooltip_array['hotspot_item'] as  $product) {
        if (isset($product['tagging_items'])) {
            foreach ($product['tagging_items'] as $hotspot ) {
                $index++;
            }
        }
    }
}

// The video flag will decide the assets and content loaded later on
$isVideo = isset($tooltip_array['type']) && $tooltip_array['type'] === 'video';

?>
<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <meta charset="utf-8">
        <title>TagCash</title>
        <link href="/css/style.css?v=18" media="screen" rel="stylesheet" type="text/css" />
        <link href="/css/responsive.css?v=5" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>

        <?php if ($isVideo) { ?>

            <script src="js/jquery.videoControls.js?v=1" type="text/javascript" charset="utf-8"></script>
            <script type="text/javascript" src="js/videoPlayerSetup.js?v=1"></script>
            <script type="text/javascript" src="js/videoPlayer.js?v=1"></script>
            <script type="text/javascript" src="js/videoAnglePluggin.js?v=1"></script>
            <script type="text/javascript" src="js/videoTagPluggin.js?v=1"></script>
            <script type="text/javascript" src="js/videoMarkerPluggin.js?v=1"></script>


            <link rel="stylesheet" type="text/css" href="css/videoPlayer.css?v=1">
            <style>

                .buy_button { display: block; width: 174px; height: 34px; background-image: url(https://www.tagcash.tv/images/201401/btn-hotspot-buy-background.png); }
                .buy_button:hover { background-position: 0px -34px; }
                .buy_button:active { background-position: 0px -68px; }

                .tag_detail_wrapper { border: 1px solid #323232; background: #fff; padding: 10px; position: absolute; top: -175px; left: -20px; z-index: 9999; width: 300px; }
                .video_tag_detail .image_wrapper { float: left; width: 118px; height: 118px; border: 1px solid #bfbfc1; overflow: hidden; }
                .video_tag_detail .text_wrapper { float: left; margin-left: 10px; width: 120px; height: 100px; overflow: hidden; }

                .imgTag { cursor: pointer; }
            </style>

        <?php } ?>

    </head>
    <body>
    <?php if (!$isVideo) { ?>
        <script>
            $( window ).resize(function() {
                Resize();
            });

            function Resize()
            {
                <?php foreach ($tooltip_array['hotspot_item'] as  $hotspot) {
                        if($hotspot['tagging_items']) {	?>
                            Relocate("<?php echo $hotspot['id']; ?>");
                <?php } } ?>
            }

            function Relocate(ele)
            {
                var x = $( "#"+ele ).position();

                //default
                $( "#bob_"+ele ).css('top','-52px');
                $( "#bob_"+ele ).css('left','-150px');

                var half_box = 164;
                var content_w = $('#pro1').width();

                //left site
                var v_left = x.left ;
                if(half_box > x.left)
                {
                    $( "#bob_"+ele ).css('left','-'+v_left+'px');
                }

                //right site
                var v_right = half_box - (content_w - half_box - x.left + 42 ) ;
                var c_right = content_w  - x.left ;
                if(half_box > c_right)
                {
                    $( "#bob_"+ele ).css('left', '-'+v_right+'px');
                }

            }

            jQuery( document ).ready(function() {

                $('.peritem').on('click touchend', function(e){
                    var item_id = $(this).attr('rel');
                    var sectorParent = $(this).parent().parent().parent();
                    sectorParent.find('.padd_box').hide();
                    sectorParent.find('#hot' + item_id).fadeIn();
                    sectorParent.find('.peritem').removeClass('active');
                    $(this).addClass('active');
                });

                var visibility = 'hidden';
                $( ".pro1 img.main_img" ).click(function(e) {
                    $( '.boxi_hov img.coin' ).css('visibility', function(i, visibility) {
                        return (visibility == 'visible') ? 'hidden' : 'visible';
                    });
                }).children().click(function(e) {
                  return false;
                });

                $(".tol_im2 img.coin").on('click touchend',function(){
                    $('li .peritem').removeClass('active');
                    $('li .peritem').each(function(index){
                        if($(this).text()=='1'){
                            $(this).addClass('active');
                        } else {
                            $(this).removeClass('active');
                        }
                    });
                });

                $( ".pro1" ).mouseenter(function() {

                    $( '.boxi_hov img.coin' ).css("visibility", "visible");
                    })
                    .mouseleave(function() {
                    $( '.boxi_hov img.coin' ).css("visibility", "hidden");
                });

                $( ".tp_box" ).mouseover(function() {

                    $( '.boxi_hov img.coin' ).css("visibility", "hidden");
                    })
                    .mouseout(function() {
                    $( '.boxi_hov img.coin' ).css("visibility", "visible");
                });

                Resize();
            })
        </script>
    <?php } ?>

        <div class="view_header">
            <div class="row">
                <div class="view_logo">
                    <a href="index.html">
                    <img src="/images/201401/logo.png" alt="" />
                    </a>
                </div>
            </div>
            </div><!--header end-->

            <div id="content" class="pad_top">

                <div id="hotspot_page">

                    <?php if ($isVideo) { ?>
                        <div id="pro1" class="pro1">
                            <div class="video_player_box" id="video_player_box1"></div>
                        </div>
                    <?php } else { ?>
                        <div id="pro1" class="pro1"style="background-image: url(<?php echo $tooltip_array['thumb_url_orignal'];?>);">
                            <?php

                            foreach ($tooltip_array['hotspot_item'] as  $hotspot) {
                                if($hotspot['tagging_items']) {

                                    $coor_x1    = $hotspot['coor_x1']*100;
                                    $coor_y1    = $hotspot['coor_y1']*100;
                                    ?>

                                    <!--<div class="tool">-->
                                    <div id="<?php echo $hotspot['id']; ?>" class="tol_im2" style="left:<?php echo $coor_x1; ?>%;top:<?php echo $coor_y1; ?>%;">

                                        <div class="boxi_hov"  rel="<?php echo $hotspot['id'] ?>">
                                            <img class="coin" src="/images/tag_icon.png" style="visibility:hidden"/>

                                            <div id="bob_<?php echo $hotspot['id'] ?>" class="tp_box_content">
                                <span class="tp_box tp_small">
								<?php $display="block"; ?>
                                    <?php $class="active"; ?>
                                    <?php foreach ($hotspot['tagging_items'] as $key => $tagelement) { ?>

                                        <div id="hot<?php echo $tagelement['id'] ?>"  class="padd_box" style="display:<?php echo $display; ?>">

                                            <div class="toltip_ima">
                                                <img src="<?php echo $tagelement['imageurl'] ?>"/>
                                            </div>

                                            <div class="item_info">
                                                <p class="item_store"><?php echo $tagelement['merchant'] ?></p>
                                                <p class="item_name"><?php echo $tagelement['name'] ?></p>
                                                <p class="item_price"><?php echo $tagelement['price'] ?> USD</p>

                                                <p class="item_btn_buy">
                                                    <a target="_blank"  href="<?php echo $tagelement['link'] ?>" ></a>
                                                    <!-- <a href"<?php echo $tagelement['link'] ?>"></a>-->
                                                </p>

                                                <div class="clr"></div>
                                            </div>

                                        </div>

                                        <?php $display="none"; ?>
                                    <?php } ?>

                                </span>
                                                <div class="item_pag">
                                                    <?php if(count($hotspot['tagging_items']) > 1) { ?>
                                                        <?php $i = 1; ?>


                                                        <ul>
                                                            <?php foreach ($hotspot['tagging_items'] as $key => $tagelement) { ?>
                                                                <li id="page-item" class="peritem <?php echo $class;?>" rel="<?php echo $tagelement['id'] ?>"><?php echo $i ?></li>
                                                                <?php $i++; ?>
                                                                <?php $class=""; ?>
                                                            <?php } ?>
                                                        </ul>


                                                    <?php } ?>
                                                </div>


                                                <div class="clr"></div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- </div>-->

                                <?php }
                            }
                            ?>
                            <img class="main_img" src="<?php echo $tooltip_array['thumb_url_orignal'];?>" alt="tagcash user" >
                        </div>
                    <?php } ?>

                    <div id="media_info" <?php if ($isVideo) { ?> style="width: 240px;" <?php } ?>>
                        <div id="who_when_pic">
                            <img class="user_avatar" src="<?php echo $tooltip_array['thumb_avatar'];?>" alt="tagcash user">
                            <div id="who_when">
                                <div id="who"><?php echo $tooltip_array['name'];?></div>
                                <div id="when"><?php echo $tooltip_array['duration'];?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="title"><?php echo $tooltip_array['content'];?></div>
                        <div id="like_comm_tags">
                            <?php echo $tooltip_array['total_like'];?>&nbsp;Likes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $tooltip_array['total_comment'];?>&nbsp;Comment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo count($tooltip_array['hotspot_item']);?>&nbsp;Tags
                        </div>
                    </div>
                    </div><!--main part end-->

                <?php if ($isVideo) { ?>
                    <div id="debug"></div>
                <?php } ?>

                    <div class="main_but">

                        <div id="downloadtheapp_call2action">
                            Download the App
                        </div>


                        <div id="gettheapp_buttons">
                            <a href="https://itunes.apple.com/us/app/tagcash/id718605786?mt=8" target="_blank" title="Download Tagcash on AppStore">
                            <div id="gettheapp_button_mac" class="gettheapp_button">
                            </div>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=tv.tagcash" target="_blank" title="Download Tagcash on GooglePlay">
                            <div id="gettheapp_button_android" class="gettheapp_button">
                            </div>
                            </a>
                        </div>

                    </div>


                </div>

    <?php if ($isVideo) { ?>
        <script>
            var vpSetup1 = new VideoPlayerSetup();
            vpSetup1.setId("video_player_box1");
            vpSetup1.setSrc("<?php echo $tooltip_array["video_url"]; ?>");
            vpSetup1.setPoster("<?php echo $tooltip_array["thumb_url"]; ?>");
            vpSetup1.setAutoPlay(false);
            vpSetup1.setIsMyVideo(true);
            vpSetup1.setPlayAfterSeek(false);
            vpSetup1.setAutoResize(true);

            var video1 = new VideoPlayer(vpSetup1);

            var tag_json1 = [
                <?php if(is_array($tooltip_array["hotspot_item"]) && count($tooltip_array["hotspot_item"]) > 0) { ?>
                <?php $tags_js = array(); ?>
                <?php foreach($tooltip_array["hotspot_item"] as $vtag) { ?>
                <?php
                    $path_items = array();

                    foreach($vtag["video_tags"] as $tag_item) {
                        $path_items[] = '{ time: '.$tag_item['time'].',  visible: '.$tag_item['visible'].', x: '.$tag_item['x1'].',  y: '.$tag_item['y1'].' }';
                    }

                    $tag = $vtag["tagging_items"][0];
//					print_r($tag);
                    if($tag) {
                        $tags_js[] = '		  { tag_id: "tag'.$vtag['id'].'",
                                           detail_visibility: false,
                                           image: "'.htmlspecialchars($tag['imageurl'], ENT_QUOTES).'",
                                           name: "'.htmlspecialchars($tag['name'], ENT_QUOTES).'",
                                           merchant: "'.htmlspecialchars($tag['merchant'], ENT_QUOTES).'",
                                           price: "'.htmlspecialchars($tag['price'], ENT_QUOTES).'",
                                           url: "'.htmlspecialchars($tag['link'], ENT_QUOTES).'",
                                           path: ['.implode(",", $path_items).']
                                          }';
                    }
                ?>
                <?php } ?>
                <?php echo implode(",", $tags_js); ?>
                <?php } ?>


            ];

            function tagClickCb(tagId)
            {
                //   alert("tagClickCb " + tagId);
            }


            var tagPluggin1 = new VideoTagPluggin(tag_json1, tagClickCb);
            video1.api_addPluggin(tagPluggin1);
            video1.api_savePlugginsSnapshot();

            var marker_json1 = {marker: [
                {marker_start: "1.28", marker_duration: "5.35", marker_type: "text", marker_text: "text plain no more then 75 chars on the markeup link maybe more chars yes it text plain no more then 75 chars on the markeup link maybe more chars yes ittext plain no more then 75 chars on the markeup link maybe more chars yes ittext plain no more then 75 chars on the markeup link maybe more chars yes it text plain no more then 75 chars on the markeup link maybe more chars yes ittext plain no more then 75 chars on the markeup link maybe more chars yes ittext plain no more then 75 chars on the markeup link maybe more chars yes it text plain no more then 75 chars on the markeup link maybe more chars yes ittext plain no more then 75 chars on the markeup link maybe more chars yes ittext plain no more then 75 chars on the markeup link maybe more chars yes it text plain no more then 75 chars on the markeup link maybe more chars yes ittext plain no more then 75 chars on the markeup link maybe more chars yes ittext plain no more then 75 chars on the markeup link maybe more chars yes it text plain no more then 75 chars on the markeup link maybe more chars yes ittext plain no more then 75 chars on the markeup link maybe more chars yes it"},
                {marker_start: "8.98", marker_duration: "10.35", marker_type: "link", marker_text: "http://www.evz.ro/pamantul-vazut-din-spatiu-imagini-fascinante-surprinse-din-satelit-galerie-foto.html"},
                {marker_start: "40.85", marker_duration: "7.3", marker_type: "text",  marker_text: "text plain no more then 75 chars on the markeup link maybe more chars yes it"}
            ]};
            //  var markerPlugin1 = new VideoMarkerPluggin(video1, marker_json1);
            //  video1.api_addPluggin(markerPlugin1);
            //{"marker":[{"marker_start":1.98,"marker_duration":2.35},{"marker_start":4.85,"marker_duration":7.3}]}

            //??
            video1.api_savePlugginsSnapshot();

            $(document).ready(function() {
                var h = window.innerHeight
                    || document.documentElement.clientHeight
                    || document.getElementsByTagName('body')[0].clientHeight;
                var w = window.innerWidth
                    || document.documentElement.clientWidth
                    || document.getElementsByTagName('body')[0].clientWidth;
//alert(w+" - "+h)
                video1.resize(vpSetup1,w,h);
            });
        </script>
    <?php } ?>


            </body>
       </html>
        
        

