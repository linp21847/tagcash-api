<?php
	error_reporting(E_ERROR );
	require_once '../config.php';
	if (!$_GET['option']) {
		$hashtags = DB::queryAllLists("SELECT * FROM hashtags");
	} else if ($_GET['option'] == 'daily') {
		$hashtags = DB::queryAllLists("SELECT * FROM hashtags WHERE `search_option` = 'daily'");
	} else if ($_GET['option'] == 'full') {
		$hashtags = DB::queryAllLists("SELECT * FROM hashtags WHERE `search_option` = 'full'");
	}
	echo json_encode($hashtags);
?>