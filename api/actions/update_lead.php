<?php
	error_reporting(E_ERROR );
	require_once '../config.php';
	$params = array();

	$id = $_GET['id'];

	if (!$id) {
		echo json_encode(array(status => 'bad', msg => 'ID should not be empty.'));
		exit;
	}
	
	$source = $_GET['source'];
	if ($source) {
		$params['source'] = $source;
	}
	
	$pose = $_GET['pose'];
	if ($pose) {
		$params['pose'] =  $pose;
	}

	$lookbook = $_GET['lookbook'];
	if ($lookbook) {
		$params['lookbook'] =  $lookbook;
	}

	$chictopia = $_GET['chictopia'];
	if ($chictopia) {
		$params['chictopia'] =  $chictopia;
	}

	$hashtag = $_GET['hashtag'];
	if ($hashtag) {
		$params['hashtag'] = $hashtag;
	}

	$preserve = $_GET['preserve'];
	if ($preserve) {
		$params['preserve'] = $preserve;
	}


	$result = DB::update('leads', $params, 'id="' . $id . '"');
	echo json_encode(array(status => 'ok', msg => 'Successfully updated.'));
?>