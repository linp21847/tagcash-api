<?php
	error_reporting(E_ERROR );
	require_once '../config.php';
	$params = array();

	$id = $_GET['id'];
	if (!$id) {
		echo json_encode(array(status => 'bad', msg => 'ID should not be empty.'));
		exit;
	}

	$result = DB::delete('contacts', 'id="' . $id . '"');
	echo json_encode(array(status => 'ok', msg => 'Successfully deleted.'));
?>