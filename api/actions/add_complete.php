<?php
	error_reporting(E_ERROR );
	require_once '../config.php';
	$params = array();
	
	$source_network = $_GET['source_network'];
	if ($source_network) {
		$params['source_network'] = $source_network;
	}

	$email = $_GET['email'];
	if ($email) {
		$params['email'] = $email;
	}

	$main_network = $_GET['main_network'];
	if ($main_network) {
		$params['main_network'] = $main_network;
	}
	
	$instagram = $_GET['instagram'];
	if (isset($instagram)) {
		$params['instagram'] =  $instagram;
	}

	$twitter = $_GET['twitter'];
	if (isset($twitter)) {
		$params['twitter'] =  $twitter;
	}

	$facebook = $_GET['facebook'];
	if (isset($facebook)) {
		$params['facebook'] =  $facebook;
	}

	$retailer = $_GET['retailer'];
	if ($retailer) {
		$params['retailer'] = $retailer;
	}

	$note = $_GET['note'];
	if ($note) {
		$params['note'] = $note;
	}

	$country = $_GET['country'];
	if ($country) {
		$params['country'] = $country;
	}

	$result = DB::insert('completes', $params);
	echo json_encode(array(status => 'ok', msg => 'Successfully added.'));
?>
