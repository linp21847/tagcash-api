<?php
    // error_reporting(E_ERROR);
    require_once '../config.php';

    function addLead($lead) {
        $params = array();

        $name = $lead['name'];
        if ($name) {
            $params['name'] = $name;
        }

        $source = $lead['source'];
        if ($source) {
            $params['source'] = $source;
        }

        $pose = $lead['pose'];
        if ($pose) {
            $params['pose'] = $pose;
        }

        $lookbook = $lead['lookbook'];
        if ($lookbook) {
            $params['lookbook'] = $lookbook;
        }

        $chictopia = $lead['chictopia'];
        if ($chictopia) {
            $params['chictopia'] = $chictopia;
        }

        $preserve = $lead['preserve'];
        if ($preserve) {
            $params['preserve'] = $preserve;
        }

        $hashtag = $lead['hashtag'];
        if ($hashtag) {
            $params['hashtag'] = $hashtag;
        }

        $result = DB::insert('leads', $params);
    }

    function addLeadsFromCSV($leads) {
        foreach ($leads as $key => $value) {
            if (empty($value['name'])) {
                continue;
            }
            addLead($value);
        }
    }

    function addHashtag($hashtag) {
        $params = array();
        if ($hashtag['name'])
            $params['name'] = $hashtag['name'];

        $result = DB::insert('hashtags', $params);
    }

    function addHashtagsFromCSV($hashtags) {
        foreach ($hashtags as $key => $value) {
            if (empty($value['name']))
                continue;

            addHashtag($value);
        }
    }


    $csv = array();

    // check there are no errors
    if($_FILES['file']['error'] == 0){
        // var_dump($_FILES);
        $name = $_FILES['file']['name'];
        $ext = strtolower(end(explode('.', $_FILES['file']['name'])));
        $type = $_FILES['file']['type'];
        $tmpName = $_FILES['file']['tmp_name'];

        // check the file is a csv
        if($ext === 'csv'){
            if(($handle = fopen($tmpName, 'r')) !== FALSE) {
                // necessary if a large csv file
                set_time_limit(0);

                $row = 0;

                if ($_POST['option'] == 'tagcash') {
                    while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                        // number of fields in the csv
                        $col_count = count($data);

                        // get the values from the csv
                        $csv[$row]['name'] = $data[0];
                        $csv[$row]['source'] = $data[1];
                        $csv[$row]['pose'] = $data[2];
                        $csv[$row]['lookbook'] = $data[3];
                        $csv[$row]['chictopia'] = $data[4];
                        $csv[$row]['preserve'] = $data[5];
                        $csv[$row]['hashtag'] = $data[6];

                        // inc the row
                        $row++;
                    }
                } else if ($_POST['option'] == "hashtag") {
                    while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                        // number of fields in the csv
                        $col_count = count($data);

                        // get the values from the csv
                        $csv[$row]['name'] = $data[0];

                        // inc the row
                        $row++;
                    }
                }

                    
                fclose($handle);
            }
        } else {
            echo "CSV file should be uploaded.";
        }
    } else {
        var_dump($_FILES);
    }
    if ($_POST['option'] == "tagcash") {
        addLeadsFromCSV($csv);
    } else if ($_POST['option'] == "hashtag") {
        addHashtagsFromCSV($csv);
    }
    

    echo "Successfully imported.";
?>