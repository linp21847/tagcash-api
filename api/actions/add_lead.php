<?php
	error_reporting(E_ERROR );
	require_once '../config.php';

	$name = $_GET['name'];
	$source = $_GET['source'];
	$pose = $_GET['pose'];
	if (!$pose) {
		$pose = 0;
	}

	$lookbook = $_GET['lookbook'];
	if (!$lookbook) {
		$lookbook = 0;
	}

	$chictopia = $_GET['chictopia'];
	if (!$chictopia) {
		$chictopia = 0;
	}

	$hastag = $_GET['hashtag'];
	$preserve = $_GET['preserve'];

	$result = DB::insert('leads', array(
			'name' => $name,
			'source' => $source,
			'pose' => $pose,
			'lookbook' => $lookbook,
			'chictopia' => $chictopia,
			'hashtag' => $hashtag,
			'preserve' => $preserve
		));
	echo json_encode(array(status => 'ok', msg => 'Successfully added.'));
?>