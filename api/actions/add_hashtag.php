<?php
	error_reporting(E_ERROR );
	require_once '../config.php';
	$params = array();
	$flag = false;
	
	$name = $_GET['name'];
	if ($name) {
		$flag = true;
		$params['name'] = $name;
	}

	$pic = $_GET['picture_count'];
	if ($pic) {
		$params['picture_count'] = $pic;
	}

	$contact = $_GET['contact_count'];
	if ($contact) {
		$params['contact_count'] = $contact;
	}

	$option = $_GET['option'];
	if ($option) {
		$params['search_option'] = $option;
	}

	if ($flag) {
		$result = DB::insert('hashtags', $params);
		echo json_encode(array(status => 'ok', msg => 'Successfully added.', value => $result));
	} else {
		echo json_encode(array(status => 'ok', msg => 'You should enter parameters.', value => 0));
	}
		
?>
