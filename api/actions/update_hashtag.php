<?php
	error_reporting(E_ERROR );
	require_once '../config.php';
	$params = array();

	$hashtag = $_GET['hashtag'];
	$date = $_GET['date'];
	$option = $_GET['option'];

	if (!$hashtag) {
		echo json_encode(array(status => 'bad', msg => 'Hashtag can not be empty.'));
		exit;
	}
	
	if ($date) {
		if ($date == "NULL") {
			$params['updated_at'] = null;
		} else {
			$params['updated_at'] = $date;
		}
	}
	
	if ($option) {
		$params['search_option'] = $option;
	}

	$result = DB::update('hashtags', $params, 'name="' . $hashtag . '"');
	echo json_encode(array(status => 'ok', msg => 'Successfully updated.'));
?>