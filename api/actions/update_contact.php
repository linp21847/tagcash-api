<?php
	error_reporting(E_ERROR );
	require_once '../config.php';
	$params = array();

	$id = $_GET['id'];

	if (!$id) {
		echo json_encode(array(status => 'bad', msg => 'ID should not be empty.'));
		exit;
	}
	
	$source_network = $_GET['source_network'];
	if ($source_network) {
		$params['source_network'] = $source_network;
	}

	$email = $_GET['email'];
	if ($email) {
		$params['email'] = $email;
	}
	
	$instagram = $_GET['instagram'];
	if ($instagram) {
		$params['instagram'] =  $instagram;
	}

	$twitter = $_GET['twitter'];
	if ($twitter) {
		$params['twitter'] =  $twitter;
	}

	$facebook = $_GET['facebook'];
	if ($facebook) {
		$params['facebook'] =  $facebook;
	}

	$retailer = $_GET['retailer'];
	if ($retailer) {
		$params['retailer'] = $retailer;
	}

	$note = $_GET['note'];
	if ($note) {
		$params['note'] = $note;
	}

	$country = $_GET['country'];
	if ($country) {
		$params['country'] = $country;
	}

	$result = DB::update('contacts', $params, 'id="' . $id . '"');
	echo json_encode(array(status => 'ok', msg => 'Successfully updated.'));
?>