-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2015 at 04:27 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tagcash-api`
--

-- --------------------------------------------------------

--
-- Table structure for table `iconosquares`
--

CREATE TABLE IF NOT EXISTS `iconosquares` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hashtag` varchar(50) NOT NULL,
  `picture_url` text NOT NULL,
  `profile` text NOT NULL,
  `followers` varchar(10) DEFAULT NULL,
  `medias` varchar(10) DEFAULT NULL,
  `blog` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `facebook` text,
  `facebook_count` varchar(10) NOT NULL,
  `twitter` text,
  `twitter_count` varchar(10) NOT NULL,
  `instagram` text,
  `instagram_count` varchar(10) NOT NULL,
  `posted_at` varchar(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `iconosquares`
--

