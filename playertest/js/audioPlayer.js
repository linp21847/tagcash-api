function AudioPlayer(aps)
{
  //console.log(angle_json);
  
  this.debug = true;
  console.log("audioPlayer constructor");
  this.pluggins = [];
  
  var me = this;
 // this.video_el = null;//15
  this.audio_el = null;
  this.id = aps.id;
  this.myHtmlElem = document.getElementById(this.id);

  this.playbtn       = null;

  this.seekslider    = null; 
  this.curtimetext   = null; 
  this.durtimetext   = null;
  this.mutebtn       = null;
  this.volumeslider  = null; 

  this.bufferedPercent = 0;
  this.muted = null;
  this.aud_canplaythrough = false;
  
  
    var audio_player_el = document.getElementById(me.id);
  //audio element    
  var audio_e = document.createElement('audio');
 
  audio_e.setAttribute("id", "audio" + this.id);    
  audio_e.setAttribute("width", "640");
  audio_e.setAttribute("height", "360");
  audio_e.setAttribute("preload", "auto");
  //audio_e.setAttribute("data-framerate", "24");
  
  if(aps.autoplay == true) audio_e.setAttribute("autoplay", true);
  
  audio_e.setAttribute("volume", "0.6");
  if (aps.src) audio_e.setAttribute("src", aps.src);
  //if (vps.poster) audio_e.setAttribute("poster", vps.poster);
  audio_e.setAttribute("type", "audio/mp3") 
  //audio_e.setAttribute("codecs", "avc1.42E01E, mp4a.40.2");
  audio_e.addEventListener("timeupdate",   function() { me.seektimeupdate();}, false); 
  
  audio_e.addEventListener("ended", function() {me.audioEnded();}, false);
 
  
  audio_e.addEventListener("loadedmetadata", function()
  {
     if(me.debug) console.log("handler = loadedmetadata");
  }, false);
  
  audio_e.addEventListener("canplaythrough", function(){
	 //if(me.debug) console.log("handler = canplaythrough");
     me.aud_canplaythrough = true;     
  }, false);
  
  
  this.audio_el = audio_e;
  $(audio_e).on("progress", function() {me.updateBuffers();});

  //disable right click on video for dowwnload
  $(audio_e).on('contextmenu',function() { return false; });
  
  audio_player_el.appendChild(audio_e); 
  //construct the audio player
  var table_cell = ["td_play", "td_fr", "td_ff", "td_slow", "td_bracketology",
                      "td_videogame", "td_seekslider", "td_curtimetext", 
                      "td_volumeslider", "td_volumebtn", "td_fullscreenbtn"];
    
  

  var audio_controls_bar = document.createElement('div');
  audio_controls_bar.setAttribute("id", "audio_controls_bar" + this.id);
  
  audio_controls_bar.setAttribute("style", "position: absolute; width: 640px; bottom: 0; left:0; right:0;  color: white;  font-size: .8em; width: 100%; min-height: 34px; padding: 0px; background:url(img/bg.png); z-index: 2147483647;");
    
  var controls_table = document.createElement('table'); //create table for controls
  controls_table.setAttribute("border","0");
  controls_table.setAttribute("width","100%");
  controls_table.setAttribute("style","align: center;");
    
  var tr = document.createElement('tr'); //create TR for the controls table
    
  i = 0;
  do
  {
      var td = document.createElement('td');
      if(i == 0) //play button
      {
		  td.style.width = "25px";
		  this.playbtn = document.createElement("button");

		  this.playbtn.setAttribute("id", "playpausebtn" + this.id);
	      this.playbtn.setAttribute("class", "playpausebtn");
		  
          this.playbtn.onclick = function() {me.playPause()};
          
          if(aps.autoplay == true)
            this.playbtn.style.background = "url(img/pause.png)";
          
		  td.appendChild(this.playbtn); //add PLAY/PAUSE button to its TD
		  
	  }
	  else if( i==1) //playprevframe
	  {
          continue;
          td.style.width = "25px";
		  this.playprevframe = document.createElement("button");
		  this.playprevframe.setAttribute("id", "playprevframe" + this.id);
          this.playprevframe.setAttribute("class", "playprevframe");
          this.playprevframe.onclick = function() {me.playPrevFrame()};
		  td.appendChild(this.playprevframe); //==> add PREV frame button to its td
	  }
	  else if( i==2) //playnextframe
	  {
          continue;
          td.style.width = "25px";
		  this.playnextframe = document.createElement("button");
		  this.playnextframe.setAttribute("id", "playnextframe" + this.id);
          this.playnextframe.setAttribute("class", "playnextframe");
          this.playnextframe.onclick = function() {me.playNextFrame()};
		  td.appendChild( this.playnextframe); //==> add NEXT frame button to its TD
	  }	  
	  
	  else if( i==3) //slow
	  {
          continue;
          td.style.width = "25px";
		  this.playslow = document.createElement("button");
		  this.playslow.setAttribute("id", "playslow" + this.id);
          this.playslow.setAttribute("class", "playslow");
          this.playslow.onclick = function() {me.playSlowFrame()};
		  td.appendChild(this.playslow);
	  }		  
	  else if( i==4) //bracketology
	  {	  
		  continue;
		  if(aps.myvideo)
          {
            this.bracketology = document.createElement("button");
		    this.bracketology.setAttribute("id", "bracketology" + this.id);
            this.bracketology.setAttribute("class", "bracketology");
            this.bracketology.onclick = function() {me.addToBracketology()};
            td.style.width = "25px";
		    td.appendChild( this.bracketology);
          }
      }
	  else if( i==5) //videogame
	  {	  
		  continue;
		  if(aps.myvideo)
          {		  
            this.videogame = document.createElement("button");
		    this.videogame.setAttribute("id", "videogame" + this.id);
            this.videogame.setAttribute("class", "videogame");
            this.videogame.onclick = function() {me.addToVideogame()};
            td.style.width = "25px";
            td.appendChild( this.videogame);
          }
      }      
      else if( i==6) //td_seekslider
	  {
		  this.seekslider = document.createElement("input");
		  this.seekslider.setAttribute("id", "seekslider" + this.id);
          //this.seekslider.style.width = "220px";
		  this.seekslider.setAttribute("class", "seekslider");
		  this.seekslider.setAttribute("type", "range");
		  this.seekslider.setAttribute("min", "0");
		  this.seekslider.setAttribute("max", "100");
		  this.seekslider.setAttribute("value", "0");
		  this.seekslider.setAttribute("step", "1");
		  
		  this.seekslider.addEventListener("change", function() {me.audSeek()}, false);
		  this.seekslider.addEventListener("input", function() {me.audSeek()}, false);
          if(this.playafterseek == true)
          {          
		     this.seekslider.addEventListener("mousedown", function(){  
                  me.audio_el.pause();
                 }, false);
		     this.seekslider.addEventListener("mouseup", function(){
                  me.audio_el.play();                  
                  me.playbtn.style.background = "url(img/pause.png)";
               }, false);
          }               
		  td.appendChild( this.seekslider);
	  }
      else if( i==7) //td_curtimetext
	  {	  
	      this.curtimetext = document.createElement("span");
	      this.curtimetext.setAttribute("id", "curtimetext" + aps.id);
	      this.curtimetext.setAttribute("style", "font-family: Arial; font-size: .9em;");      
	      this.curtimetext.innerHTML = "00:00";
	      td.appendChild( this.curtimetext);
	      
	      var delimitator = " / ";
	      td.innerHTML = td.innerHTML + delimitator;
	      
	      this.durtimetext = document.createElement("span");
	      this.durtimetext.setAttribute("id", "durtimetext" + aps.id);
	      this.durtimetext.setAttribute("style", "font-family: Arial; font-size: .9em;");
	      this.durtimetext.innerHTML = "00:00";
	      td.style.width = "80px";
          td.appendChild( this.durtimetext);
      }
      else if( i==9) //td_volumeslider
      {
		  this.volumeslider = document.createElement("input");
		  this.volumeslider.setAttribute("id", "volumeslider" + aps.id);
		  this.volumeslider.setAttribute("class", "volumeslider");
		  this.volumeslider.setAttribute("type", "range");
		  this.volumeslider.setAttribute("min", "0");
		  this.volumeslider.setAttribute("max", "100");
		  this.volumeslider.setAttribute("value", "0");
		  this.volumeslider.setAttribute("step", "1");
		  
		  this.volumeslider.addEventListener("change",function(){me.setVolume()},false);
		  this.volumeslider.addEventListener("input", function(){me.setVolume()} ,false);
		  
          //initial volume
          var initialVolume = 0.6;
          $(this.volumeslider).css('background-image',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + initialVolume + ', #A2A2A2), '
                + 'color-stop(' + initialVolume + ', #222222)  '
                + ')'
                );
          this.volumeslider.value = 0.5 * 100;
          audio_e.volume = 0.5;		
            
		  td.style.width = "60px";
          td.appendChild( this.volumeslider);
	  }
	  
	  else if( i==8)  //td_volumebtn
	  {
		  this.mutebtn = document.createElement("button");
		  this.mutebtn.setAttribute("id", "volumebtn" + this.id);
          this.mutebtn.setAttribute("class", "volumebtn");
          this.mutebtn.onclick = function() {me.muteVolume()};
          td.style.width = "25px";
          td.appendChild( this.mutebtn);		  
	  }
      
      else if( i==10)  //td_fullscreenbtn
	  {
		 continue;
		  this.fullscreenbtn = document.createElement("button");
		  this.fullscreenbtn.setAttribute("id", "fullscreenbtn" + this.id);
          this.fullscreenbtn.setAttribute("class", "fullscreenbtn");
          this.fullscreenbtn.onclick = function() {me.toggleFullScreen()};
          td.style.width = "25px";
          td.appendChild( this.fullscreenbtn);		  
	  }
	  //add this td to the tr
      tr.appendChild(td); // add one td at time to tr
  } while(++i < table_cell.length)
    
  controls_table.appendChild(tr); //add tr to table
    
  audio_controls_bar.appendChild(controls_table); //append table to the controls div
  audio_player_el.appendChild(audio_controls_bar);    
    
  if (aps.autoplay == true) audio_e.play();  
 }

AudioPlayer.prototype.updateBuffers = function()
{
    var aud = this.audio_el;
    
    var buffered = aud.buffered;
    if (buffered.length > 0) {
       var width = (buffered.end(buffered.length - 1) - buffered.start(0)) / aud.duration * 100;
       this.bufferedPercent = width;
    }    
    
}
AudioPlayer.prototype.setVolume = function()
{               
  var aud = this.audio_el;
  var volumeslider = this.volumeslider;
  aud.volume = this.volumeslider.value / 100;
  
  $(volumeslider).css('background-image',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + aud.volume + ', #A2A2A2), '
                + 'color-stop(' + aud.volume + ', #222222)'
                + ')'
  );                
};

AudioPlayer.prototype.audSeek = function ()
{
	//if(this.debug) console.log("audSeek");
    var aud = this.audio_el;
	var seekto = aud.duration * (this.seekslider.value / 100);
	aud.currentTime = seekto;
};

AudioPlayer.prototype.muteVolume = function()
{
	var aud = this.audio_el;

   	if(aud.muted){
		aud.muted = false;
        this.mutebtn.style.background = "url(img/vol.png)";
        if(this.debug) console.log("Mute action. Current volume=" + aud.volume);
	} else {
		aud.muted = true;
        this.mutebtn.style.background = "url(img/mute.png)";
	}
   
};
AudioPlayer.prototype.seektimeupdate = function ()
{
    //return;
    var aud = this.audio_el;
	//console.log("seektimeupdate " + this.id + " " + aud.currentTime);
	
    var nt = aud.currentTime * (100 / aud.duration);
	this.seekslider.value = nt; // 0-100% percent value
	var curmins = Math.floor(aud.currentTime / 60);
	var cursecs = Math.floor(aud.currentTime - curmins * 60);
	var durmins = Math.floor(aud.duration / 60);
	var dursecs = Math.floor(aud.duration - durmins * 60);
	if(cursecs < 10){ cursecs = "0"+cursecs; }
	if(dursecs < 10){ dursecs = "0"+dursecs; }
	if(curmins < 10){ curmins = "0"+curmins; }
	if(durmins < 10){ durmins = "0"+durmins; }
	
    curtimetext = document.getElementById("curtimetext" + this.id);
    curtimetext.innerHTML = curmins + ":" + cursecs;
    durtimetext = document.getElementById("durtimetext" + this.id);
    durtimetext.innerHTML = durmins + ":" + dursecs;
    
    var val = nt/100;

    var w = this.seekslider.parentNode.offsetWidth;
    var val2 = nt * w / 100;// + 1;
    var val3 = this.bufferedPercent * w/100;
    this.seekslider.setAttribute("style", 
                       "background-color: #000000; " + 
                       "background-image: url(img/played.png), url(img/buffered.png);" + 
                       "background-position: left top, left top; " + 
                       "background-repeat: no-repeat; " + 
                       "background-size: " + val2 + "px 7px, " + val3 + "px 7px; "
                      );
};

AudioPlayer.prototype.setSrc = function(src) {
    if(this.debug) console.log("AudioPlayer.setSrc=" + src);
    //this.src = src;
    var me_id = this.me_id;
    var aud = document.getElementById("audio" + me_id);
    aud.src = src;
    aud.play();    
};

AudioPlayer.prototype.audioEnded = function() 
{ 
    if(this.debug) console.log("audio ended");
    this.playbtn.style.background = "url(img/play.png)";
    
    var aud = this.audio_el; 
    aud.currentTime = 0;
};
AudioPlayer.prototype.api_pauseAudio = function() {  
   this.audio_el.pause();
};
AudioPlayer.prototype.api_playAudio = function(startFrom, duration) {  
    //startFrom == 0 and duration == -1 play entire video
    console.log("api_playAudio startFrom:" + startFrom + " for duration: " + duration);
    var aud = this.audio_el;
    
    var b = (function(aud, startFrom, duration){
		return function () {
		    if(aud.currentTime >= (startFrom + duration))
		     aud.pause();		    
	      }
		})(aud, startFrom, duration);
    
    if((duration > 0 ) && (startFrom >=0) )
    {
       console.log("api_playVideo unbind and bin timeupdate for: " + this.audio_el.id);
       //$("#" + this.video_el.id).unbind("timeupdate");
       //$("#" + this.video_el.id).bind("timeupdate", b );

       $(this.audio_el).off("timeupdate");
       $(this.audio_el).on("timeupdate", b );       
    }
    
    aud.play();
};
AudioPlayer.prototype.api_setCurrentTimeSrc = function(crt_time, play_audio) {    
    if(this.debug) 
       console.log("AudioPlayer.api_setCurrentTimeSrc to: "+ crt_time);
    
    var me = this;    
    this.audio_el.addEventListener("canplay",function() {         
        me.audio_el.currentTime = crt_time; //orig
        //me.video_el.src = me.video_el.src + "#t=" + crt_time;
        if(play_audio == true)
          me.audio_el.play();
        //remove myself after trigger
        me.audio_el.removeEventListener("canplay", arguments.callee, false);
        }, false);
    //me.video_el.currentTime = crt_time;
};
AudioPlayer.prototype.api_setCurrentTime = function(crt_time, play_audio) {    
    console.log("AudioPlayer.api_setCurrentTime to: "+ crt_time);
    
    var me = this;
    this.audio_el.addEventListener("canplay",function() {         
        me.audio_el.currentTime = crt_time;
        if(play_audio == true) me.audio_el.play();
        //remove myself after trigger
        me.audio_el.removeEventListener("canplay", arguments.callee, false);
    }, false);

    me.audio_el.currentTime = crt_time;
};
AudioPlayer.prototype.api_getAudioDuration = function() { 
  if (this.audio_el) return this.audio_el.duration;
  return null;  
}
AudioPlayer.prototype.api_getCurrentTime = function()
{
   var crtTime = this.audio_el.currentTime;
   //if(this.debug) console.log("VideoPlayer.api_getCurrentTime:" + crtTime);
   return crtTime; 
};
AudioPlayer.prototype.api_addPluggin = function(p) { 
  console.log("api_addPluggin");
  p.createHtml(this);
  p.hookIt(this);
  
  this.pluggins.push(p);
}
AudioPlayer.prototype.api_reloadPlugginMetadata = function() { 
  console.log("api_reloadPlugginMetadata");

  for(var i=0; i<this.pluggins.length; i++) 
  {
    this.pluggins[i].reloadMetadata(this);
  } 
}
AudioPlayer.prototype.playPause = function(){    
    
    if(this.debug) console.log("AudioPlayer.playPause id=" + this.id); 
    
	var aud = this.audio_el;

    if(aud.paused){
		if(this.debug) console.log("play id=" +  this.id);
        aud.play();
		this.playbtn.style.background = "url(img/pause.png)";
        //this.playprevframe.disabled = "disabled";
        //this.playnextframe.disabled = "disabled";
        
        //stop timer for tags
        //if (myClass.animateTimer != null) clearTimeout(myClass.animateTimer); 
        
	} 
    else 
    {
		if(this.debug) console.log("paused id=" +  this.id);
        aud.pause();
		this.playbtn.style.background = "url(img/play.png)";
        //this.playprevframe.disabled = "";
        //this.playnextframe.disabled = "";
	}
};

AudioPlayer.prototype.api_savePlugginsSnapshot = function() { 
  console.log("api_savePlugginsSnapshot");

  for(var i=0; i<this.pluggins.length; i++) 
  {
    this.pluggins[i].saveSnapshot(this);
  } 
}


AudioPlayer.prototype.api_restorePlugginsSnapshot = function() { 
  console.log("api_restorePlugginsSnapshot");

  for(var i=0; i<this.pluggins.length; i++) 
  {
    this.pluggins[i].restoreSnapshot(this);
  } 
}
AudioPlayer.prototype.api_getPlayerVersion = function() {
    return "audio player 1.0";
};
AudioPlayer.prototype.api_addListener = function(audioListener, function_name) {
	if(me.debug) console.log("api_addListener for: " + audioListener);
    this.audio_el.addEventListener(audioListener, function_name, false);
};
