function VideoPlayerSetup()
{
  this.debug = true;
  if(this.debug) console.log("videoPlayerSetup constructor");
  
  this.id = null
  this.src = null;
  this.width = 640;
  this.height = 360;
  this.minimumWidth = 420; // the minimum width when autoresizing, it won't get less than this
  this.minimumHeight = 240; // the minimum height when autoresizing, it won't get less than this
  this.poster = null; //need to find a generic image in poster is not defined
  this.autoplay = false;
  this.ismyvideo = false;
  this.showangles = true;
  this.angle_json = {"angle":[]};
  this.playafterseek = true;
  this.defaultplayback = 1;
  this.tags = null;
  this.add_tags = false;
  this.is3D = false;
  this.autoResize = false;  

}
/*
VideoPlayerSetup.prototype.set = function()
{
  if(this.debug)
    console.log("set");
};
*/
VideoPlayerSetup.prototype.setId = function(id)
{
  if(this.debug)
    console.log("setId");
  this.id = id;  
};
VideoPlayerSetup.prototype.setIs3D = function(value)
{
  if(this.debug)
    console.log("setIs3D");
  this.is3D = value;  
};

VideoPlayerSetup.prototype.setWidth = function(width) {
	this.width = width;
}
VideoPlayerSetup.prototype.setHeight = function(height) {
	this.height = height;
}
VideoPlayerSetup.prototype.setMinimumWidth = function(minimumWidth) {
	this.minimumWidth = minimumWidth;
}
VideoPlayerSetup.prototype.setMinimumHeight = function(minimumHeight) {
	this.minimumHeight = minimumHeight;
}
VideoPlayerSetup.prototype.setAutoResize = function(autoResize) {
	this.autoResize = autoResize;
}


VideoPlayerSetup.prototype.setSrc = function(src)
{
  if(this.debug)
    console.log("setSrc");
  this.src = src;  
};

VideoPlayerSetup.prototype.setPoster = function(poster)
{
  if(this.debug)
    console.log("setPoster");
  this.poster = poster; 
};

VideoPlayerSetup.prototype.setAutoPlay = function(autoplay)
{
  if(this.debug)
    console.log("setAutoplay");
  this.autoplay = autoplay;  
};

VideoPlayerSetup.prototype.setIsMyVideo = function(ismyvideo)
{
  if(this.debug)
    console.log("setIsMyVideo");
  this.ismyvideo = ismyvideo;  
};
VideoPlayerSetup.prototype.setShowAngles = function(showangles)
{
  if(this.debug)
    console.log("setShowAngles");
  this.showangles = showangles;
};
VideoPlayerSetup.prototype.setAngles = function(angles)
{
  if(this.debug)
    console.log("setAngles");
  this.angle_json = angles;
};
VideoPlayerSetup.prototype.setPlayAfterSeek = function(playafterseek)
{
  if(this.debug)
    console.log("setPlayAfterSeek");
  this.playafterseek = playafterseek;
};
VideoPlayerSetup.prototype.setDefaultPlayback = function(defaultplayback)
{
  if(this.debug)
    console.log("setDefaultPlayback");
  this.defaultplayback = defaultplayback;
};

VideoPlayerSetup.prototype.setTags = function(tags_json)
{
  if(this.debug)
    console.log("setTags");
  this.tags = tags_json;
};

VideoPlayerSetup.prototype.setAddTags = function(add_tags)
{
  if(this.debug)
    console.log("setAddTags");
  this.add_tags = add_tags;
};


