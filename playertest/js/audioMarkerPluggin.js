
function AudioMarkerPluggin(as_main, audio_markers_json)
{
  console.log("AudioMarkerPluggin.constructor");
  this.debug = true;
  var me = this;
  this.audio_markers = audio_markers_json;
  this.audio_main = as_main;
  this.audio_duration = null;
  
  $("#audio"+as_main.id).on("loadedmetadata", function()
  {
     if(me.debug) 
        console.log("METADATA LOADED VideoMarkerPluggin !!!!!!   duration: "  + as_main.api_getAudioDuration());
     me.audio_duration = as_main.api_getAudioDuration();   
  });
  this.slider_length = 420;//288; //px
  this.start_offset = 24;//px from the start of the player until the seeker
}


AudioMarkerPluggin.prototype.createHtml = function(vp) 
{
  console.log("AudioMarkerPluggin.createHtml");
  var me = this; 
  me.audio_duration = vp.api_getAudioDuration();
  console.log("audio_duration=" +  me.audio_duration);
}

AudioMarkerPluggin.prototype.hookIt = function(vp) 
{
  console.log("AudioMarkerPluggin.hookIt");
  
  var me = this;      
  vp.audio_el.addEventListener("timeupdate", function() {me.timeupdateMarkers(vp)}, false);
}

AudioMarkerPluggin.prototype.saveSnapshot = function(vp) 
{
  console.log("AudioMarkerPluggin.saveSnapshot");
}  

AudioMarkerPluggin.prototype.restoreSnapshot = function(vp) 
{
  console.log("AudioMarkerPluggin.restoreSnapshot");
}  


AudioMarkerPluggin.prototype.reloadMetadata = function(vp) 
{
  console.log("AudioMarkerPluggin.reloadMetadata");
}  

AudioMarkerPluggin.prototype.timeupdateMarkers = function(vp) 
{
  console.log("AudioMarkerPluggin.timeupdateMarkers vp.id="+vp.id);
  var seekslider = document.getElementById("seekslider" + vp.id);
  var currTime = vp.api_getCurrentTime();
  var background_image = seekslider.style.backgroundImage;
  //console.log("background_image"); console.log(background_image);
  
  var background_position = seekslider.style.backgroundPosition;
  //console.log("background_position"); console.log(background_position);
  
  var background_size = seekslider.style.backgroundSize;
  //console.log("background_size"); console.log(background_size);
    
  var me = this;
  
  if((me.audio_markers["marker"] != null) && (me.audio_markers["marker"].length > 0))
  { 
	  for(var l=0; l< me.audio_markers["marker"].length; l++)
	  {	
          //console.log(me.audio_markers["marker"][l].marker_start);          
          //console.log(me.audio_markers["marker"][l].marker_duration);
          //console.log(currTime);
          var pos = Math.floor((me.slider_length * me.audio_markers["marker"][l].marker_start) / me.audio_duration);
          var size = Math.floor((me.slider_length * me.audio_markers["marker"][l].marker_duration) / me.audio_duration);
          
          background_image = "url(img/marker.png), " + background_image;
          //background_position = "50px 0px, " + background_position ;
          //background_size = "20px 7px, " + background_size;
          
          background_position = pos +"px 0px, " + background_position ;
          background_size = size +"px 7px, " + background_size;
          
          //show marker div
          if(
              (Number(currTime) > Number(me.audio_markers["marker"][l].marker_start)) && 
              (Number(currTime) < Number(me.audio_markers["marker"][l].marker_start) + Number(me.audio_markers["marker"][l].marker_duration))
            )
          {
              var link = '<a href="' + me.audio_markers["marker"][l].marker_text +'" ><div style="color:aqua;  text-decoration: none; background-color: none;">'+ me.audio_markers["marker"][l].marker_text +'</div></a>';
              var vpb = vp.id;              

 //             $("#" + vpb +"").find("#seekslider" + vp.id).after('<div id="marker" style="position: absolute; height:30px;' +                                
 //                                                               ' top: -35px; display: inline-block; background-color: #444444; border : 2px solid #1b1b1b; ' + 
 //                                                               ' z-index: 100;        -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)"; filter: alpha(opacity=50); opacity:.5;'+
 //                                                               ' "> '+link+'</div>');

              $("#" + vpb +"").find("#marker_" + l + "_"+ vp.id).remove();
              $("#" + vpb +"").find("#seekslider" + vp.id).after('<div id="marker_' + l + "_"+ vp.id + '" class="bubble">'+link+'</div>');
              $("#" + vpb +"").find("#marker_" + l + "_"+ vp.id).css("left", Number(pos   + me.start_offset ) +"px");
          }
          else
          {
			 //console.log("hide marker !!!!!!!!!!!!!!!  " + "#marker_" + l + "_"+ vp.id);
			 $("#marker_" + l + "_"+ vp.id).empty().remove();
		  }
         
      }
  }
  seekslider.setAttribute("style", 
                       "background-color: #000000; " + 
                       "background-image: " + background_image + "; " +
                       "background-position: " + background_position +  "; " +
                       "background-repeat: no-repeat; " + "; " +
                       "background-size: " + background_size + "; "
                      );   
  
}

