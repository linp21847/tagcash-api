function VideoAnglePluggin(video_angles_json)
{
  console.log("VideoAnglePluggin.constructor");

  this.video_angles = video_angles_json;
  
  this.oldCrtTime = 0;
  this.playAngleNow = false;  
}


VideoAnglePluggin.prototype.setVideoAngles = function(video_angles_json) 
{
  console.log("VideoAnglePluggin.setVideoAngles");
  this.video_angles = video_angles_json;
}   


VideoAnglePluggin.prototype.createHtml = function(vp) 
{
  console.log("VideoAnglePluggin.createHtml");

  var add_angle_div = document.createElement("div");
  add_angle_div.setAttribute("id","top_add_angle" + vp.id);
  add_angle_div.setAttribute("style"," position: absolute; top:0px; right:0px; font-family: Arial;" +
                                     " font-size: .8em; padding-left:8px; padding-right:8px; height: 26px; width: 38px; " + 
                                     " color: white; opacity:0.8; filter:alpha(opacity=80); background-color: #63ca00; z-index: 2147483647;");
                                     
  var add_angle_button = document.createElement("input");
  add_angle_button.setAttribute("type", "button");
  add_angle_button.setAttribute("value", "Add");
  add_angle_div.appendChild(add_angle_button);
  vp.myHtmlElem.appendChild(add_angle_div);



  var angle_div = document.createElement("div");
  angle_div.setAttribute("id","top_angle" + vp.id);
  angle_div.setAttribute("style"," position: absolute; top:0px; left:0px; font-size: .7em; " + 
                                   " padding-left:8px; padding-right:8px; color: white; " + 
                                   " opacity:0.8; filter:alpha(opacity=80); z-index: 2147483647;");
                                   
  var angle_table = document.createElement("table");
  angle_table.setAttribute("border", "0");
  angle_table.setAttribute("cellpadding", "0");
  angle_table.setAttribute("cellspacing", "0");
  var angle_table_tr = document.createElement("tr");
    
    
  //return button
  angle_table_back_td = document.createElement("td");
  angle_table_back_td.id = "back_" + vp.id;
  var btn = document.createElement("img");
  btn.src = "img/cangleback.png";
  btn.id = "btn_back_" + vp.id;
  angle_table_back_td.appendChild(btn);
    
  var me = this;
  btn.onclick = (function(angle_table_back_td) { 
                return function() {
                   console.log("Back");
                   angle_table_back_td.style.visibility = "hidden";
                   $(".angle_button").css("background", "url(img/cangle.png)");

                   me.playAngleNow = false;
                   var vid = vp.video_el;
                   vid.pause();    
                   vid.removeEventListener("timeupdate", me.timeupdateAngle, false);
                   vid.removeEventListener("ended",      me.restoreOrigVideo, false);
            
                   //vid.addEventListener("timeupdate", vp.seektimeupdate,false); 
                   //vid.addEventListener("ended", vp.videoEnded, false);
      
                   vid.src = vp.src + "#t=" + me.oldCrtTime;
                   vp.api_restorePlugginsSnapshot();

                   //console.log("NOW PLAY THE VIDEO SRC:" + vid.src);

                   vid.load(); //cc
                   vid.playbackRate = vp.playbackRate;
                   if(vp.muted == true)
                     vid.muted = true;
                   else
                     vid.muted = false;
                       
                   vid.play();                                       
			   };
    })(angle_table_back_td);

    angle_table_tr.appendChild(angle_table_back_td);	
    angle_table_back_td.style.visibility = "hidden";
    
        
    
    
    console.log("nr_angles left:" + me.video_angles["angle"].length);
    
    //add all angles
	if((me.video_angles["angle"] != null) && (me.video_angles["angle"].length > 0))
	{ 
	  for(var l=0; l< me.video_angles["angle"].length; l++)
	  {				
		 //console.log("add td for left angle l=" + l );
		 var angle_table_td = document.createElement("td");
		 //angle_table_td.innerHTML = "Angle" + l;
		 angle_table_td.id = "angle" + l + "_" + vp.id;

         var btn_div = document.createElement("div");
         btn_div.className = "angle_button";
         btn_div.setAttribute("style", 
                              " background: url(img/cangle.png); background-repeat:no-repeat; background-position:center;" + 
                              " width: 32px; height: 24px;");
         var label = document.createElement("span");
         var dur = Number(me.video_angles["angle"][l].duration).toFixed(0);//cc
         label.innerHTML = dur;//Number(l) + 1;

         if(dur<10)
         {
            label.setAttribute("style", "position: relative; top:4px; left:10px; font-family: Arial; font-size: .8em;");
         }
         else
         {
            label.setAttribute("style", "position: relative; top:4px; left:8px; font-family: Arial; font-size: .8em;"); 
         }
         btn_div.appendChild(label); 

         btn_div.onclick = (function(l, d, id) { 
            return function() {
               $(".angle_button").css("background", "url(img/cangle.png)");

               d.setAttribute("style", 
                              " background: url(img/cangleON.png); background-repeat:no-repeat; background-position:center;" + 
                              " width: 32px; height: 24px;");
               var angle_table_back_td = document.getElementById("back_" + vp.id);
               angle_table_back_td.style.visibility = "visible";
               me.playAngleNow = true;
               
               me.oldCrtTime = vp.api_getCurrentTime();
               me.angleEndTime = Number(me.video_angles["angle"][l].angle_start) + Number(me.video_angles["angle"][l].duration);
               console.log("Angle start: " + me.video_angles["angle"][l].angle_start + " dur:" +  me.video_angles["angle"][l].duration);
               console.log("angleEndTime " + me.angleEndTime);
               
               vp.api_savePlugginsSnapshot();
               vp.changeSrcTo(vp.id, me.video_angles["angle"][l].video_id, me.video_angles["angle"][l].angle_start , me.video_angles["angle"][l].duration );              
 
               vp.api_reloadPlugginMetadata();
			};
	     })(l, btn_div, vp.id);
         
         angle_table_td.appendChild(btn_div);
                      
		 angle_table_td.style.visibility = "hidden";
         angle_table_tr.appendChild(angle_table_td); 
         //console.log(angle_table_td);                 
	   }//end for all left angles
	}//end if angle left

 
  angle_table.appendChild(angle_table_tr); //add tr to table
  angle_div.appendChild(angle_table); //add table to div video_player_box
  vp.myHtmlElem.appendChild(angle_div);
}


VideoAnglePluggin.prototype.hookIt = function(vp) 
{
  console.log("VideoAnglePluggin.hookIt");
  
  var me = this;
      
  vp.video_el.addEventListener("timeupdate", function() {me.timeupdateAngle(vp)}, false);
  vp.video_el.addEventListener("ended",      function() {me.restoreOrigVideo(vp)}, false);  
}



VideoAnglePluggin.prototype.timeupdateAngle = function(vp) 
{
  //console.log("VideoAnglePluggin.timeupdateAngle");

  var currTime = vp.api_getCurrentTime();

  if ((this.playAngleNow) && (this.angleEndTime <= currTime))
  {
     console.log("Angle is over");

     var angle_table_back_td = document.getElementById("back_" + vp.id);
     angle_table_back_td.style.visibility = "hidden";

     this.restoreOrigVideo(vp);
     vp.api_restorePlugginsSnapshot();

     this.playAngleNow = false;
     return;
  } 

  
  if((this.video_angles["angle"] != null) && (this.video_angles["angle"].length > 0))
  { 
	   for(var l=0; l< this.video_angles["angle"].length; l++)
	   {
         if (this.playAngleNow)
         {
            $("#angle" + l + "_" + vp.id).css("visibility" , "hidden");	 
         }   
         else 
         {
    	    var clip_obj = this.video_angles["angle"][l];
	        //console.log(clip_obj);
		    var duration = clip_obj.duration;
		    var angle_start = clip_obj.angle_start;
		    var video_id = clip_obj.video_id;
		    var master_start = clip_obj.master_start;
		    var end_clip = Number(master_start) + Number(duration);
		    if((currTime >= master_start) && (currTime <= end_clip))
		    {
	          $("#angle" + l + "_" + vp.id).css("visibility" , "visible");
	        }
	        else
            {
		      $("#angle" + l + "_" + vp.id).css("visibility" , "hidden");	 
		    }
	     }
      }   
  }
}


VideoAnglePluggin.prototype.restoreOrigVideo = function(vp) 
{
  console.log("VideoAnglePluggin.restoreOrigVideo");

  if (!this.playAngleNow) return;

  var vid = vp.video_el;
  vid.pause();
     
  vid.src = vp.src + "#t=" + this.oldCrtTime;
  vid.load();   

  vid.removeEventListener("timeupdate", function() {me.timeupdateAngle(vp)}, false);
  vid.removeEventListener("ended",      function() {me.restoreOrigVideo(vp)}, false);
    
  console.log("restoreOrigVideo-removeEvent done add orig event listeners");
    
  //vid.addEventListener("timeupdate", vp.seektimeupdate,false); 
  //vid.addEventListener("ended",      vp.videoEnded, false);    
   
  vid.play(); 
}



VideoAnglePluggin.prototype.reloadMetadata = function(vp) 
{
  console.log("VideoAnglePluggin.reloadMetadata");
}  

VideoAnglePluggin.prototype.saveSnapshot = function(vp) 
{
  console.log("VideoAnglePluggin.saveSnapshot");
}  

VideoAnglePluggin.prototype.restoreSnapshot = function(vp) 
{
  console.log("VideoAnglePluggin.restoreSnapshot");
}  


