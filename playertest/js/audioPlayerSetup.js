function AudioPlayerSetup()
{
  this.debug = true;
  if(this.debug) console.log("AudioPlayerSetup constructor");
  
  this.id = null
  this.src = null;
  //this.poster = null; //need to find a generic image in poster is not defined
  this.autoplay = false;
  this.ismyaudio = false;
  //this.showangles = true;
  //this.angle_json = {"angle":[]};
  this.playafterseek = true;
  //this.defaultplayback = 1;
  //this.tags = null;
  //this.add_tags = false;
}
AudioPlayerSetup.prototype.setId = function(id)
{
  if(this.debug)
    console.log("setId");
  this.id = id;  
};
AudioPlayerSetup.prototype.setSrc = function(src)
{
  if(this.debug)
    console.log("setSrc");
  this.src = src;  
};
AudioPlayerSetup.prototype.setAutoPlay = function(autoplay)
{
  if(this.debug)
    console.log("setAutoplay");
  this.autoplay = autoplay;  
};
AudioPlayerSetup.prototype.setIsMyVideo = function(ismyvideo)
{
  if(this.debug)
    console.log("setIsMyVideo");
  this.ismyaudio = ismyaudio;  
};
AudioPlayerSetup.prototype.setPlayAfterSeek = function(playafterseek)
{
  if(this.debug)
    console.log("setPlayAfterSeek");
  this.playafterseek = playafterseek;
};
