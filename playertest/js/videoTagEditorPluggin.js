function VideoTagEditorPluggin()
{
  console.log("VideoTagEditorPluggin.constructor");

  this.video_tags = [];
  this.isPlaying = false;
  this.snapshot = null;

  this.tag_canvas = null;
  this.ctx = null;

  this.imgTag = new Image();
  this.imgTag.src = "img/tag24.png";
  this.imgTag.id = "base_tag";
  
  this.selectorPosX = 100;
  this.selectorPosY = 100;
  this.selectorVisible = true;
  
  this.path = [{"x":100,"y":100,"time":0,"visible":1}];

  this.handler_w = this.handler_h = 40;
  this.click_w = this.click_h = 12;

  this.mouseMoved = 0;
  this.touchMoved = 0;

  $("#dump").html(JSON.stringify(this.path));
}

VideoTagEditorPluggin.prototype.setVideoTags = function(video_tags_json) 
{
  console.log("VideoTagEditorPluggin.setVideoTags");
  this.video_tags = video_tags_json;
}

VideoTagEditorPluggin.prototype.addNewTag = function(vp) 
{
  console.log("VideoTagEditorPluggin.addNewTag");
  this.selectorPosX = 100;
  this.selectorPosY = 100;
  this.selectorVisible = 1;
  
  this.path = [{"x":100,"y":100,"time":0,"visible":1}];
  //vp.api_setCurrentTime(0, false);
  this.timeupdateTags(vp);

  $("#dump").html(JSON.stringify(this.path)); 
} 

VideoTagEditorPluggin.prototype.createHtml = function(vp) 
{
  console.log("VideoTagEditorPluggin.createHtml");
  
  var me = this;
  
  this.tag_canvas = document.createElement("canvas");
  this.ctx = this.tag_canvas.getContext("2d");
  this.tag_canvas.setAttribute("id", "tag_canvas_" + vp.id);
  this.tag_canvas.setAttribute("width", "640");
  this.tag_canvas.setAttribute("height", "330");
  this.tag_canvas.setAttribute("style"," position: absolute; top:0px; left:0px; " +
                              " z-index: 100; "); //border:1px solid red;");
      
  vp.myHtmlElem.appendChild(this.tag_canvas);
  this.drawHandler();
  
}

VideoTagEditorPluggin.prototype.canvasMouseDown = function(vp, e) 
{
  if(!vp.video_el.paused) return;
  var me = this;

  var x = e.layerX;
  var y = e.layerY;
  //console.log(e); 
  //console.log("canvasMouseDown " + x + " " + y);
  var hitRes = this.userDown(vp, e, x, y);
  //console.log(hitRes);

  if (hitRes == "hit")
  {
    this.mouseMoved = 1;
    this.tag_canvas.onmousemove = function(e) { me.canvasMouseMove(vp, e)};      
  }
  else if (hitRes == "checkHit"){
         
  }
  else //noHit
  {
  }
}


//return "noHit, hit, checkHit"
VideoTagEditorPluggin.prototype.userDown = function(vp, e, x, y) 
{
  var crtTime = vp.api_getCurrentTime();

  //console.log("userDown " + x + " " + y + " " + crtTime);
  var me = this;
  
  
  //find the handler position for this crtTime
  var hx = hy = -1; //center of the handler
  if (this.path.length == 1)
  {
     hx = this.selectorPosX;
     hy = this.selectorPosY;
  }
  else 
  {
    var found = false;
    for(var i=1; i<this.path.length; i++)
    {
      var p0 = this.path[i-1];
      var p1 = this.path[i];
    
      if ((crtTime >= Number(p0.time)) && (crtTime <= Number(p1.time)))
      {
        //console.log("Found interval"); console.log(p0); console.log(p1); 
        var dt = (crtTime - p0.time) / (p1.time - p0.time);
        hx = p0.x + (p1.x - p0.x) * dt;
        hy = p0.y + (p1.y - p0.y) * dt;
        found = true;
        break;
     }
   }
   
   if (!found) {
     lastPoint = this.path[this.path.length-1];
     hx = lastPoint.x;
     hy = lastPoint.y;     
   }
  } 
  
  //console.log("hx=" + hx + ", hy=" + hy);
  
  //test for hit
  if ((hx >= x-this.handler_w/2) && (hx <= x+this.handler_w/2) && 
      (hy >= y-this.handler_h/2) && (hy <= y+this.handler_h/2))
  {
    //console.log("Hit handler");
   
    //extra test for check
    var dx = x - hx;
    var dy = hy - y;
    //console.log("dx=" + dx + ", dy="+ dy);
    if ((dx > 5) && (dy > 5))
    {
       var isVisible = this.selectorVisible ? 0: 1;
       //console.log("Hit on check, selector visible= " + this.selectorVisible + " next visible=" + isVisible);
       this.addToPath(vp, hx, hy, isVisible);
       this.selectorVisible = !this.selectorVisible;
       this.drawHandler(vp);
       
       //this.updateHandlerPosition(crtTime, vp.video_el.paused);
       return "checkHit";        
       $("#dump").html(JSON.stringify(this.path));   
    }  

    return "hit";
  } 
  
  return "noHit";   
}



VideoTagEditorPluggin.prototype.canvasMouseUp = function(vp, e) 
{

  var x = e.layerX;
  var y = e.layerY;

  if (this.mouseMoved == 0) return;
  //console.log("canvasMouseUp " + x + " " + y);
  
  this.tag_canvas.onmousemove = null;
  
  this.userUp(vp, e, x, y);
  this.mouseMoved = 0; 
}


VideoTagEditorPluggin.prototype.userUp = function(vp, e, x, y) 
{
  //console.log("userUp " + x + " " + y);

  this.selectorPosX = x;
  this.selectorPosY = y; 
  
  if ((this.path[0].x == 100) && (this.path[0].y == 100) && (this.path[0].time == 0))
  {
    this.path = [];
  }

  var isVisible = this.selectorVisible ? 1 : 0;
  this.addToPath(vp, x, y, isVisible);
  this.convertPathToTags();
}

VideoTagEditorPluggin.prototype.getCrtPath = function() 
{
  return this.path;
}



VideoTagEditorPluggin.prototype.canvasMouseMove = function(vp, e) 
{

  this.mouseMoved = 1;
  x = e.layerX;
  y = e.layerY;
  
  console.log("canvasMouseMove " + x + " " + y);
  
  this.selectorPosX = x;
  this.selectorPosY = y;
  
  this.ctx.clearRect(0, 0, 640, 330);   
  this.drawHandler();  
}


VideoTagEditorPluggin.prototype.canvasTouchStart = function(vp, e) 
{
  if(!vp.video_el.paused) return;
  var me = this;
  e.preventDefault();
  
  var x = e.targetTouches[0].pageX - getOffsetLeft(this.tag_canvas);
  var y = e.targetTouches[0].pageY - getOffsetTop(this.tag_canvas);
  
  console.log("canvasTouchStart x:" + x + ",y: " + y);

  var hitRes = this.userDown(vp, e, x, y);
  //console.log(hitRes);
  
  if (hitRes == "hit")
  {
    this.touchMoved = 1;   
    this.selectorPosX = x;
    this.selectorPosY = y;
    
    this.tag_canvas.addEventListener('touchmove', function(event) {
       me.canvasTouchMove(vp, event); 
    }, false);
  }
  else if (hitRes == "checkHit"){
         
  }
  else //noHit
  {
  }
}


VideoTagEditorPluggin.prototype.canvasTouchMove = function(vp, e) 
{
  if(!vp.video_el.paused) return;
  this.touchMoved = 1;
  var me = this;
  e.preventDefault();
  var x = e.targetTouches[0].pageX - getOffsetLeft(this.tag_canvas);
  var y = e.targetTouches[0].pageY - getOffsetTop(this.tag_canvas);

  this.selectorPosX = x;
  this.selectorPosY = y;
  
  this.ctx.clearRect(0, 0, 640, 330);   
  this.drawHandler(); 
}

VideoTagEditorPluggin.prototype.canvasTouchEnd = function(vp, e) 
{
  if(!vp.video_el.paused) return;
  var me = this;
  e.preventDefault();

  if (this.ontouchmove == 0) return;
  
  this.tag_canvas.removeEventListener('touchmove', function(event) {
     me.canvasTouchMove(vp, event); 
  }, false);
  
  this.userUp(vp, e, this.selectorPosX, this.selectorPosY);
  this.touchMoved = 0; 
}




VideoTagEditorPluggin.prototype.addToPath = function(vp, x, y, visible) 
{
  //console.log("addToPath " + x + " " + y);
  var crtTime = vp.api_getCurrentTime();
  
  var found = false;
  for(var i=0; i<this.path.length; i++)
  {
     var p = this.path[i];
     if (p.time == crtTime)
     {
        p.x = x;
        p.y = y;
        p.visible = visible;
        found = true;
        break;
     }
  }
  
  if (!found) 
  {
    this.path.push({
      x: x,
      y: y,
      time: crtTime,
      visible: visible
    });
    
    this.path.sort(function(a, b){
      return (a.time - b.time);
    });
  }  
/*
  for(var i=0; i<this.path.length; i++)
  {
    var s = this.path[i];
    console.log("x=" + s.x + " y=" + s.y + " t=" + s.time + " visible=" + s.visible);
  } 
*/ 
}

//[{tag_id: "tag1",  x0: 0,  y0: 5,  x1: 340,   y1:  130,  t0: 2,   t1: 10}, ....]
VideoTagEditorPluggin.prototype.convertPathToTags = function(vp) 
{
  //console.log("VideoTagEditorPluggin.convertPathToTags");

  this.video_tags = []; //clear old tags

  this.path.sort(function(a, b){
      return (a.time - b.time);
  });
  
  //console.log("After time sort");
 // for(var i=0; i<this.path.length; i++)
 // {
   // var s = this.path[i];
   // console.log("x=" + s.x + " y=" + s.y + " t=" + s.time + " visible=" + s.visible);
 // } 
    
  //console.log(this.path);  
    
  /*
  var prevPoint = this.path[0];
  for(var i=1; i<this.path.length; i++)
  {
     var s = this.path[i]; // {x: 1, y: 2, time: 3.0, visible: 0|1 }
     this.video_tags.push({
       
     })
  } 
  */ 
  
  $("#dump").html(JSON.stringify(this.path));
}

VideoTagEditorPluggin.prototype.hookIt = function(vp) 
{
  console.log("VideoTagEditorPluggin.hookIt");
  
  var me = this;
  vp.video_el.addEventListener("timeupdate", function(){me.timeupdateTags(vp)},false); 
  
  //event handlers  
  this.tag_canvas.onmousedown   = function(e) { me.canvasMouseDown(vp, e)};
  this.tag_canvas.onmouseup     = function(e) { me.canvasMouseUp(vp, e)};

  this.tag_canvas.addEventListener('touchstart', function(event) {
    //event.preventDefault();
    me.canvasTouchStart(vp, event);
  });
  
  this.tag_canvas.addEventListener('touchend', function(event) {
    //event.preventDefault();
    me.canvasTouchEnd(vp, event);
  }); 
  
  
  vp.video_el.addEventListener("loadedmetadata", function()
  {
     vp.api_setCurrentTime(0, false);
  }, false);
  /*
  vp.video_el.addEventListener("play", function()
  {
     me.isPlaying = true;
  }, false); 
  
  vp.video_el.addEventListener("play", function()
  {
     me.isPlaying = false;
  }, false); 
  */
}






VideoTagEditorPluggin.prototype.timeupdateTags = function(vp) 
{
  var vid = vp.video_el;
  var currTime = vid.currentTime;
  var me = this;

  //console.log("timeupdateTags " + currTime);
  
  
  this.ctx.clearRect(0, 0, 640, 330);   
  //if (vp.video_el.paused) 
  { 
    this.updateHandlerPosition(currTime, vp.video_el.paused);
    this.drawHandler();  
  } 
  
   
}

VideoTagEditorPluggin.prototype.drawTags = function(vp) 
{
  var vid = vp.video_el;
  var currTime = vid.currentTime;
  //console.log("drawTags " + vp.id + " " + currTime);

  var me = this;
  
  //re-trigger
  requestAnimFrame(function() { me.drawTags(vp)});  
}


VideoTagEditorPluggin.prototype.updateHandlerPosition = function(crtTime, isPause) 
{
 //console.log("VideoTagEditorPluggin.updateHandlerPosition " + crtTime);

 if (this.path.length < 2) return;
 
 this.path.sort(function(a, b){
      return (a.time - b.time);
 });
 
 
 //we have at least 2 points
 for(var i=1; i<this.path.length; i++)
 {
    var p0 = this.path[i-1];
    var p1 = this.path[i];
    
    if ((crtTime >= Number(p0.time)) && (crtTime <= Number(p1.time)))
    {
       //console.log("Found interval"); console.log(p0); console.log(p1); 
       
       var dt = (crtTime - p0.time) / (p1.time - p0.time);
       
       this.selectorPosX = p0.x + (p1.x - p0.x) * dt;
       this.selectorPosY = p0.y + (p1.y - p0.y) * dt;
       this.selectorVisible = p0.visible;
       return;
    }
 } 
 
 if (isPause)
 {
    firstPoint = this.path[0];
    lastPoint = this.path[this.path.length-1];
    
    
    if (crtTime <= Number(firstPoint.time))
    {
      this.selectorPosX    = firstPoint.x;
      this.selectorPosY    = firstPoint.y;
      this.selectorVisible = 1;
    }
    else
    {
      this.selectorPosX = lastPoint.x;
      this.selectorPosY = lastPoint.y;
      this.selectorVisible = lastPoint.visible;
    }  
 }   
}

VideoTagEditorPluggin.prototype.drawHandler = function() 
{
 var x = this.selectorPosX;
 var y = this.selectorPosY;
 
 handler_w = this.handler_w;
 handler_h = this.handler_h;
 click_w = this.click_w;
 click_h = this.click_h;
 
 this.ctx.fillStyle = 'rgba(225,225,225,0.5)';
 this.ctx.fillRect(x-handler_w/2, y-handler_h/2, handler_w, handler_h);
 //this.ctx.lineWidth="2";
 this.ctx.strokeStyle = (this.selectorVisible) ? "#00FF00": "#FF0000";
 this.ctx.fillStyle = "rgba(128, 128, 128, 1)";
 this.ctx.strokeRect(x-handler_w/2, y-handler_h/2, handler_w, handler_h);
 
 this.ctx.fillStyle = (this.selectorVisible) ? 'rgba(0,225,225,0.9)' : 'rgba(225,0,0,0.9)';
 this.ctx.fillRect(x + handler_w/2 - click_w, y-handler_h/2 , click_w, click_h); 
 this.ctx.strokeRect(x + handler_w/2 - click_w, y-handler_h/2 , click_w, click_h); 
}

VideoTagEditorPluggin.prototype.reloadMetadata = function(vp) 
{
  //console.log("VideoTagEditorPluggin.reloadMetadata");
  
  this.video_tags = []; 
}  

VideoTagEditorPluggin.prototype.saveSnapshot = function(vp) 
{
  console.log("VideoTagEditorPluggin.saveSnapshot");
  
  this.snapshot = this.video_tags;
}  

VideoTagEditorPluggin.prototype.restoreSnapshot = function(vp) 
{
 //console.log("VideoTagEditorPluggin.restoreSnapshot");

 if (this.snapshot != null) { this.video_tags = this.snapshot;}
}  



  function getOffsetLeft( elem )
  {
    var offsetLeft = 0;
    do {
      if ( !isNaN( elem.offsetLeft ) )
      {
          offsetLeft += elem.offsetLeft;
      }
    } while( elem = elem.offsetParent );
    return offsetLeft;
  }
  
  
  function getOffsetTop( elem )
  {
    var offsetTop = 0;
    do {
      if ( !isNaN( elem.offsetTop ) )
      {
          offsetTop += elem.offsetTop;
      }
    } while( elem = elem.offsetParent );
    return offsetTop;
  } 
