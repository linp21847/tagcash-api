function VideoMarker(video_marker_main, vs_main)
{  
  this.debug = true;
  if(this.debug)
      console.log("VideoMarker constructor: video_marker_main: " + video_marker_main );
  
  var me = this;
  
  if(this.debug) 
    console.log("version: " + vs_main.api_getPlayerVersion());
  
  this.crt_nr_timeline = 0;
  this.crtIndex = -1;
  
  //SUBMIT SYNC
  this.vs_addtotimeline_btn = document.getElementById("video_marker_addtotimeline"); if(this.debug)  console.log(this.vs_addtotimeline_btn);
  this.vs_addtotimeline_btn.myClass = this;  
  this.vs_addtotimeline_btn.onclick = this.addToTimelineSync;    
    
  //video
  this.main_video = vs_main;
  //JSON
  this.vs_json = {"marker": []};
  this.timeline_arr = [];

  this.master_start = 0;
  this.angle_start  = 0;
  this.duration     = 1;

  $("#video"+vs_main.id).on("loadedmetadata", function()
  {
     if(me.debug) console.log("METADATA LOADED MAIN !!!!!!   duration: "  + vs_main.api_getVideoDuration());
     me.masterMetadataLoaded();
  });

  this.max_size_marker_text = 75;
}


VideoMarker.prototype.masterMetadataLoaded = function()
{
  console.log("VideoMarker.masterMetadataLoaded");
  var me = this;

  var master_duration = Number(this.main_video.api_getVideoDuration()).toFixed(2);
 
  $("#marker_slider").slider({
         from: 0, 
         to: master_duration,
         limits: true, 
         step: 0.01, round: 2,
         dimension: '&nbsp;', 
         skin: "round_plastic",
         onstatechange: function( value ){
           var val = Number(value).toFixed(2);
           console.log("Master start " + val);
           me.master_start = val;
           me.main_video.api_setCurrentTime(val);
           if((master_duration - Number(me.master_start).toFixed(2)) <= Number(me.duration))
           {
               me.duration = master_duration - Number(me.master_start).toFixed(2);
               $("#marker_duration_slider").slider("value", Number(me.duration));
               $("#marker_duration_slider").slider().update();
           }           
         }
   });
        
   $("#marker_slider").slider("value", 0);
   $("#marker_slider").slider().update();
        
   me.master_start = 0;
//new cc   

 
        
  $("#marker_duration_slider").slider({
         from: 0, 
         to: me.getMinDuration(),//Number(myClass.angle_video.my_video.duration).toFixed(0), 
         limits: true, 
         step: 0.01, round: 2,
         dimension: '&nbsp;', 
         skin: "round_plastic",
         onstatechange: function( value ){
             var val = Number(value).toFixed(2);
             console.log("val:" + val);
             var max_dur = me.getMaxDuration();
             var curr_duration = val;
             console.log("max_dur:" + max_dur + " curr_duration:" + curr_duration + " me.master_start:" + me.master_start);
  
             if(Number(max_dur) < Number(curr_duration))
             {
                 $("#marker_duration_slider").slider("value", Number(max_dur)); return;

             }                             
             me.duration = val;
             me.main_video.api_setCurrentTime(Number(val) + Number(me.master_start));
         }          
  }); 
   //me.setSlidersValues(me.master_start,  master_duration);
}

VideoMarker.prototype.getMaxDuration = function()
{
  var max_duration = 0;
  var m_duration = Number(this.main_video.api_getVideoDuration()).toFixed(2);
  var curr_m_duration =  Number($("#marker_slider").slider("value")).toFixed(2);
  var mas = m_duration - curr_m_duration;
  max_duration = mas;
  
  console.log("getMaxDuration => max_duration: " + Number(max_duration).toFixed(2));
  return Number(max_duration).toFixed(2);
  
}

VideoMarker.prototype.getMinDuration = function()
{
  var m_duration = Number(this.main_video.api_getVideoDuration()).toFixed(2);
  return m_duration;  
}

VideoMarker.prototype.setSlidersValues = function(master_start,  duration)
{
  console.log("setSlidersValues " + master_start +  " " +  duration);
 
  //set the slider values;
  $("#marker_slider").slider("value", master_start);
  $("#marker_duration_slider").slider("value", duration); 
              
  this.main_video.api_setCurrentTime(master_start, false);                         
}

VideoMarker.prototype.clearLocalData = function()
{
   this.start_from = 0;
   this.duration = 0;
   this.sync_start = 0;
};

VideoMarker.prototype.getJsonTimeline = function ()
{
  var timeline_arr = [];
  var timeline_table = document.getElementById("video_marker_timeline_table");
          
  for(var i=1; i<timeline_table.rows.length; i++)
  {
    var r = timeline_table.rows[i];
    //console.log(r);
    
    var json_obj = new Object();
    json_obj.video_id     = r.currentSrc;
    json_obj.marker_start = Number(r.cells[0].innerHTML);;
    json_obj.marker_duration     = Number(r.cells[1].innerHTML);
    
    if(r.cells[3].childNodes[0].value == "Link")
      json_obj.marker_type = "link";
    else
      json_obj.marker_type  = "text";
    
    json_obj.marker_text = r.cells[3].childNodes[1].value;
    
    timeline_arr.push(json_obj);
  }  
  
  var json = { 
    marker: timeline_arr
  };   
  
  if(this.debug)  console.log(JSON.stringify(json));            
  
  return json;
}



VideoMarker.prototype.addToTimelineSync = function ()
{
    var vs_myClass = this.myClass;

    if(vs_myClass.debug) console.log("Master Start: " + vs_myClass.master_start + " duration: " + vs_myClass.duration);
    
    var action = "";
    if (vs_myClass.crtIndex == -1) //add new
    {
      var currentIndex = ++vs_myClass.crt_nr_timeline;    
      action = "add";
    }
    else  //update editing index
    {
      //console.log("Update index " + vs_myClass.crtIndex);
      action = "update";
    }
   
    var timeline_table = document.getElementById("video_marker_timeline_table");

    if (action == "update")
    {
      for(var i=1; i<timeline_table.rows.length; i++)
      {
        var r = timeline_table.rows[i];
        //console.log(r.cells[0].innerHTML);
        if (r.index == vs_myClass.crtIndex)
        {
          r.cells[0].innerHTML = $("#marker_slider").slider("value");
          r.cells[1].innerHTML = $("#marker_duration_slider").slider("value");
        }
      }
           
      vs_myClass.crtIndex = -1;
      return;
    }
    
    
    
    var add_timeline_tr = timeline_table.insertRow(0);

    add_timeline_tr.main_video = vs_myClass.main_video; //keep the reference to angle video
    add_timeline_tr.index = currentIndex;
    add_timeline_tr.currentSrc = vs_myClass.currentSrc;
    
    var mvideo = vs_myClass.main_video;
    var play_duration = vs_myClass.duration;
    var master_start  = vs_myClass.master_start; 

    var add_nr_td = document.createElement("td");   
    add_nr_td.innerHTML = currentIndex;    
    //add_timeline_tr.appendChild(add_nr_td);
    
    var add_start_td        = document.createElement("td");
    add_start_td.style.textAlign = "right";  
    add_start_td.innerHTML  = Number(vs_myClass.master_start).toFixed(2);
    add_timeline_tr.appendChild(add_start_td);
 
    var add_duration_td       = document.createElement("td");    
    add_duration_td.style.textAlign = "right";  
    add_duration_td.innerHTML = Number($("#marker_duration_slider").slider("value")).toFixed(2);//Number(vs_myClass.duration).toFixed(0);
    add_timeline_tr.appendChild(add_duration_td);
    
    //Play
    var add_play_angle_td = document.createElement("td");
    add_play_angle_td.innerHTML = "<img src='img/play.png' alt='Play' height='24' width='24'>";//"Play";
    add_play_angle_td.onclick = (function (vs_myClass, currentIndex ){
      return function (){          
          if(vs_myClass.debug) console.log("Play idx " + currentIndex);
          
          var table = document.getElementById("video_marker_timeline_table");
          for(var i=1; i<timeline_table.rows.length; i++)
          {
            var r = timeline_table.rows[i];
            if (r.index == currentIndex)
            {                            
              var l_master_start = Number(r.cells[0].innerHTML); 
              var l_duration     = Number(r.cells[1].innerHTML); 
              
              $("#video"+vs_myClass.main_video.id).unbind("loadedmetadata");
             
              mvideo.api_pauseVideo();              
              mvideo.api_setCurrentTime(l_master_start);
              vs_myClass.currentSrc = r.currentSrc; //cc            
              mvideo.api_playVideo(l_master_start, l_duration);
              
              //uncomment folowing code and comment the line above if you want to be sure that the clip will
              //play right away. The code check that the marker will be 100% playable
              /*
              var setPlayTimer = setInterval(function()
               {
                 if(vs_myClass.main_video.api_canPlayWithoutDelay() == true)
                 {
                     clearInterval(setPlayTimer);
                     //now both clips will play in sync
                     mvideo.api_playVideo(l_master_start, l_duration);
                     //avideo.api_playVideo(l_angle_start, l_duration);
                 }
               }, 2000
              );
              */
              break;
            }
          }
       };
    })(vs_myClass, currentIndex);
    
    //add td to tr on timeline table
    add_timeline_tr.appendChild(add_play_angle_td);    
    
    //Marker text
    var action_td        = document.createElement("td");
    action_td.style.textAlign = "left";  
    
    var select_marker_type = document.createElement("select");
    select_marker_type.name = "marker_select";
    
    var select_marker_option_link = document.createElement("option");
    select_marker_option_link.text = "Link";
    select_marker_option_link.value = "Link";
    //select_marker_type.add("Link");
    var select_marker_option_text = document.createElement("option");
    select_marker_option_text.text = "Text";
    select_marker_option_text.value = "Text";
    //select_marker_type.add("Text");
    
    var marker_text = document.createElement("input");
    marker_text.type = "text";
    marker_text.name = "marker_text";
    marker_text.id = "marker_text";    
    marker_text.size = vs_myClass.max_size_marker_text;    
    marker_text.maxlength = vs_myClass.max_size_marker_text;    
    
    select_marker_type.add(select_marker_option_link);
    select_marker_type.add(select_marker_option_text);
    
    action_td.appendChild(select_marker_type);
    action_td.appendChild(marker_text);
    
    add_timeline_tr.appendChild(action_td);
    
    
    //Edit
    var add_action_td   = document.createElement("td");
    edit_btn = document.createElement("button");
    var t=document.createTextNode("Edit");
    edit_btn.appendChild(t);
    add_action_td.appendChild(edit_btn);
    
    
    del_btn = document.createElement("button");
    var t=document.createTextNode("Delete");
    del_btn.appendChild(t);
    add_action_td.appendChild(del_btn);
    
    
    del_btn.onclick = (function (vs_myClass, currentIndex ){
      return function (){
         if(vs_myClass.debug) console.log("Delete " + currentIndex);
         var table = document.getElementById("video_marker_timeline_table");
          
         for(var i=1; i<timeline_table.rows.length; i++)
         {
           var r = timeline_table.rows[i];
           if (r.index == currentIndex)
           { 
             table.deleteRow(i);
             break;
           }  
         }
      };   
    })(vs_myClass,  currentIndex);    

    
    edit_btn.onclick = (function (vs_myClass, currentIndex ){
      return function (){
          if(vs_myClass.debug) console.log("Edit " + currentIndex);
          var table = document.getElementById("video_marker_timeline_table");
          
          for(var i=1; i<timeline_table.rows.length; i++)
          {
            var r = timeline_table.rows[i];
            if (r.index == currentIndex)
            {
              //console.log(r);
              //console.log("EDIT THIS SRC: " + r.currentSrc);
              
              vs_myClass.crtIndex = currentIndex; 
              //vs_myClass.currentSrc = r.currentSrc; 
              mvideo.api_pauseVideo();
              
              var l_master_start = Number(r.cells[0].innerHTML); 
              var l_duration     = Number(r.cells[1].innerHTML); 
              
              vs_myClass.setSlidersValues(l_master_start, l_duration);
              break;
            }
          }
       };   
    })(vs_myClass,  currentIndex);    
    
    add_timeline_tr.appendChild(add_action_td);   
    timeline_table.appendChild(add_timeline_tr);
    
    vs_myClass.crtIndex = -1;    
        
    
    //IMPORTANT
    /*
     * SAVE THE VALUE FROM vs_myClass.vs_json INTO THE DATABASE AND USE IT ON THE PLAYER
     * 
     */
    
    
};
