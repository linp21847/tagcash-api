function VideoSync(video_sync_main, video_sync_angle, vs_main, vs_angle)
{  
  this.debug = true;
  if(this.debug)
      console.log("VideoSync constructor: video_sync_main: " + video_sync_main + " video_sync_angle: " + video_sync_angle);
  
  var me = this;
  
  if(this.debug) 
    console.log("version: " + vs_main.api_getPlayerVersion());
  
  this.crt_nr_timeline = 0;
  this.crtIndex = -1;
  
  //SUBMIT SYNC
  this.vs_addtotimeline_btn = document.getElementById("video_sync_addtotimeline"); if(this.debug)  console.log(this.vs_addtotimeline_btn);
  this.vs_addtotimeline_btn.myClass = this;  
  this.vs_addtotimeline_btn.onclick = this.addToTimelineSync;    
    
  //both videos
  this.main_video = vs_main;
  this.angle_video = vs_angle;
  this.currentSrc = vs_angle.src;
  
  //JSON
  this.vs_json = {"angle": []};
  this.timeline_arr = [];

  this.master_start = 0;
  this.angle_start  = 0;
  this.duration     = 1;

  $("#video"+vs_main.id).on("loadedmetadata", function()
  {
     if(me.debug) console.log("METADATA LOADED MAIN !!!!!!   duration: "  + vs_main.api_getVideoDuration());
     me.masterMetadataLoaded();
  });

}


VideoSync.prototype.masterMetadataLoaded = function()
{
  console.log("VideoSync.masterMetadataLoaded");
  var me = this;

  var master_duration = Number(this.main_video.api_getVideoDuration()).toFixed(2);
 
  $("#master_slider").slider({
         from: 0, 
         to: master_duration,
         limits: true, 
         step: 0.01, round: 2,
         dimension: '&nbsp;', 
         skin: "round_plastic",
         onstatechange: function( value ){
           var val = Number(value).toFixed(2);
           console.log("Master start " + val);
           me.master_start = val;
           me.main_video.api_setCurrentTime(val);
           if((master_duration - Number(me.master_start).toFixed(2)) <= Number(me.duration))
           {
               me.duration = master_duration - Number(me.master_start).toFixed(2);
               $("#duration_slider").slider("value", Number(me.duration));
               $("#duration_slider").slider().update();
           }           
         }
   });
        
   $("#master_slider").slider("value", 0);
   $("#master_slider").slider().update();
        
   me.master_start = 0;
}

VideoSync.prototype.getMaxDuration = function()
{
  var max_duration = 0;
  var m_duration = Number(this.main_video.api_getVideoDuration()).toFixed(2);
  var a_duration = Number(this.angle_video.api_getVideoDuration()).toFixed(2);
  var curr_m_duration =  Number($("#master_slider").slider("value")).toFixed(2);
  var curr_a_duration =  Number($("#angle_slider").slider("value")).toFixed(2);
  var mas = m_duration - curr_m_duration;
  var ang = a_duration - curr_a_duration;
  
  if(Number(mas) >= Number(ang))
    max_duration = ang;
  else
    max_duration = mas;
  
  console.log("getMaxDuration => max_duration: " + Number(max_duration).toFixed(2));
  return Number(max_duration).toFixed(2);
  
}

VideoSync.prototype.getMinDuration = function()
{
  var min_duration = 0;
  var m_duration = Number(this.main_video.api_getVideoDuration()).toFixed(2);
  var a_duration = Number(this.angle_video.api_getVideoDuration()).toFixed(2);
  console.log("getMinDuration main_duration:" + m_duration + " angle_duration:" + a_duration);
  if (Number(m_duration) >= Number(a_duration))
  {
	min_duration = a_duration;
  }
  else if(Number(m_duration) < Number(a_duration))
  {
    min_duration = m_duration;
  }
  
  console.log("getMinDuration => " + min_duration)  ;
  return min_duration;  
}

VideoSync.prototype.angleMetadataLoaded = function()
{
  $("#video"+this.angle_video.id).unbind("loadedmetadata");

  var parent = $("#angle_slider").parent();
  //console.log(parent);
  while (parent[0].firstChild) 
  {
     parent[0].removeChild(parent[0].firstChild);
  }

  var in1 = document.createElement("input");
  in1.setAttribute("id", "angle_slider");
  in1.setAttribute("type", "text");
  in1.setAttribute("name", "area");
  in1.setAttribute("value", "0");
  parent[0].appendChild(in1);

  var me = this; 
      
  var angle_duration = Number(this.angle_video.api_getVideoDuration()).toFixed(2);
  $("#angle_slider").slider({
         from: 0, 
         to: angle_duration, 
         limits: true, 
         step: 0.01, round: 2,
         dimension: '&nbsp;', 
         skin: "round_plastic",
         onstatechange: function( value ){
           var val = Number(value).toFixed(2);
           
           console.log("Angle start " + val);
                       
           me.angle_start = val;           
           me.angle_video.api_setCurrentTime(Number(me.angle_start).toFixed(2));      
           
           if((angle_duration - Number(me.angle_start).toFixed(2)) <= Number(me.duration))
           {
               me.duration = angle_duration - Number(me.angle_start).toFixed(2);
               console.log("angle dur: " + angle_duration + " angle start: " + Number(me.angle_start).toFixed(2));
               console.log("adjust slider suration to: " + me.duration);
               $("#duration_slider").slider("value", Number(me.duration));
               $("#duration_slider").slider().update();
           }           
         }
  });
        
  $("#angle_slider").slider("value", 0);
  $("#angle_slider").slider().update(); 
         
  me.angle_start = 0;
  me.duration = 1;
               
  var parent = $("#duration_slider").parent();
  //console.log(parent);

  while (parent[0].firstChild) 
  {
     parent[0].removeChild(parent[0].firstChild);
  }

  var in1 = document.createElement("input");
  in1.setAttribute("id", "duration_slider");
  in1.setAttribute("type", "text");
  in1.setAttribute("name", "area");
  in1.setAttribute("value", "0");
  parent[0].appendChild(in1);        
        
        
  $("#duration_slider").slider({
         from: 0, 
         to: me.getMinDuration(),//Number(myClass.angle_video.my_video.duration).toFixed(0), 
         limits: true, 
         step: 0.01, round: 2,
         dimension: '&nbsp;', 
         skin: "round_plastic",
         onstatechange: function( value ){
             var val = Number(value).toFixed(2);
             console.log("val:" + val);
             var max_dur = me.getMaxDuration();
             var curr_duration = val;
             console.log("max_dur:" + max_dur + " curr_duration:" + curr_duration + " me.master_start:" + me.master_start);
  
             if(Number(max_dur) < Number(curr_duration))
{               $("#duration_slider").slider("value", Number(max_dur)); return;

}                             
             me.duration = val;
             me.main_video.api_setCurrentTime(Number(val) + Number(me.master_start));
             me.angle_video.api_setCurrentTime(Number(val) + Number(me.angle_start));             
         }          
  });  
}

VideoSync.prototype.setSlidersValues = function(master_start, angle_start, duration)
{
  console.log("setSlidersValues " + master_start + " " + angle_start  + " " +  duration);
 
  //set the slider values;
  $("#master_slider").slider("value", master_start);
  $("#angle_slider").slider("value", angle_start );
  $("#duration_slider").slider("value", duration); 
              
  this.main_video.api_setCurrentTime(master_start, false);
  this.angle_video.api_setCurrentTime(angle_start, false);                             
}


VideoSync.prototype.changeAngleSrc = function(src, setSliders, master_start, angle_start, duration)
{
  if ((setSliders == null) || (setSliders == undefined)) setSliders = false;
  
  if(this.debug) console.log("changeAngleSrc " + src + " " + setSliders);
  
  this.angle_video.api_changeVideoSrc(src);
  this.currentSrc = src;

  var me = this;
  
  $("#video"+me.angle_video.id).one("loadedmetadata", function()
  {
    if(me.debug) console.log("METADATA LOADED ANGLE !!!!!!   duration: "  + me.angle_video.api_getVideoDuration());
    
    me.angleMetadataLoaded();
    
    if (setSliders) me.setSlidersValues(master_start, angle_start, duration);
  });
}


VideoSync.prototype.clearLocalData = function()
{
   this.start_from = 0;
   this.duration = 0;
   this.sync_start = 0;
};

VideoSync.prototype.getJsonTimeline = function ()
{
  var timeline_arr = [];
  var timeline_table = document.getElementById("video_sync_timeline_table");
          
  for(var i=1; i<timeline_table.rows.length; i++)
  {
    var r = timeline_table.rows[i];
    //console.log(r);
    
    var json_obj = new Object();
    json_obj.video_id     = r.currentSrc;
    json_obj.master_start = Number(r.cells[0].innerHTML);;
    json_obj.angle_start  = Number(r.cells[1].innerHTML);
    json_obj.duration     = Number(r.cells[2].innerHTML);
    
    timeline_arr.push(json_obj);
  }  
  
  var json = { 
    angle: timeline_arr
  };   
  
  if(this.debug)  console.log(JSON.stringify(json));            
  
  return json;
}



VideoSync.prototype.addToTimelineSync = function ()
{
    var vs_myClass = this.myClass;

    if(vs_myClass.debug) console.log("Master Start: " + vs_myClass.master_start + " Angle start: " + vs_myClass.angle_start + " duration: " + vs_myClass.duration);
    
    var action = "";
    if (vs_myClass.crtIndex == -1) //add new
    {
      var currentIndex = ++vs_myClass.crt_nr_timeline;    
      action = "add";
    }
    else  //update editing index
    {
      //console.log("Update index " + vs_myClass.crtIndex);
      action = "update";
    }
   
    var timeline_table = document.getElementById("video_sync_timeline_table");

    if (action == "update")
    {
      //console.log(timeline_table);
      for(var i=1; i<timeline_table.rows.length; i++)
      {
        var r = timeline_table.rows[i];
        //console.log(r.cells[0].innerHTML);
        if (r.index == vs_myClass.crtIndex)
        {
          r.cells[0].innerHTML = $("#master_slider").slider("value");
          r.cells[1].innerHTML = $("#angle_slider").slider("value");
          r.cells[2].innerHTML = $("#duration_slider").slider("value");
        }
      }
           
      vs_myClass.crtIndex = -1;
      return;
    }
    
    
    
    var add_timeline_tr = timeline_table.insertRow(0);

    add_timeline_tr.main_video = vs_myClass.main_video; //keep the reference to angle video
    add_timeline_tr.angle_video = vs_myClass.angle_video; //keep the reference to angle video
    add_timeline_tr.index = currentIndex;
    add_timeline_tr.currentSrc = vs_myClass.currentSrc;
    
    var mvideo = vs_myClass.main_video;
    var avideo = vs_myClass.angle_video;
    var play_duration = vs_myClass.duration;
    var master_start  = vs_myClass.master_start; 
    var angle_start   = vs_myClass.angle_start;

    var add_nr_td = document.createElement("td");   
    add_nr_td.innerHTML = currentIndex;    
    //add_timeline_tr.appendChild(add_nr_td);
    
    var add_start_td        = document.createElement("td");
    add_start_td.style.textAlign = "right";  
    add_start_td.innerHTML  = Number(vs_myClass.master_start).toFixed(2);
    add_timeline_tr.appendChild(add_start_td);
    
    var add_start_sec_td       = document.createElement("td");
    add_start_sec_td.style.textAlign = "right";
    add_start_sec_td.innerHTML = Number(vs_myClass.angle_start).toFixed(2);
    add_timeline_tr.appendChild(add_start_sec_td);
    
    var add_duration_td       = document.createElement("td");    
    add_duration_td.style.textAlign = "right";  
    add_duration_td.innerHTML = Number($("#duration_slider").slider("value")).toFixed(2);//Number(vs_myClass.duration).toFixed(0);
    add_timeline_tr.appendChild(add_duration_td);
    
    //Play
    var add_play_angle_td = document.createElement("td");
    add_play_angle_td.innerHTML = "<img src='img/play.png' alt='Play' height='24' width='24'>";//"Play";
    add_play_angle_td.onclick = (function (vs_myClass, currentIndex ){
      return function (){          
          if(vs_myClass.debug) console.log("Play idx " + currentIndex);
          
          var table = document.getElementById("video_sync_timeline_table");
          for(var i=1; i<timeline_table.rows.length; i++)
          {
            var r = timeline_table.rows[i];
            if (r.index == currentIndex)
            {                            
              var l_master_start = Number(r.cells[0].innerHTML); 
              var l_angle_start  = Number(r.cells[1].innerHTML);
              var l_duration     = Number(r.cells[2].innerHTML); 
              
              $("#video"+vs_myClass.main_video.id).unbind("loadedmetadata");//
              $("#video"+vs_myClass.angle_video.id).unbind("loadedmetadata");
              
              mvideo.api_pauseVideo();
              avideo.api_pauseVideo();
                
              mvideo.api_setCurrentTime(l_master_start);
                
              avideo.api_changeVideoSrc(r.currentSrc);//cc
              //vs_myClass.angle_video.api_changeVideoSrc(r.currentSrc); //cc  
              vs_myClass.currentSrc = r.currentSrc; //cc
              vs_myClass.angle_video.api_setCurrentTimeSrc(l_angle_start);//cc
              
              //avideo.api_setCurrentTime(l_angle_start); //orig

             var setPlayTimer = setInterval(function()
             {
                 if(vs_myClass.angle_video.api_canPlayWithoutDelay() == true)
                 {
                     clearInterval(setPlayTimer);
                     //now both clips will play in sync
                     mvideo.api_playVideo(l_master_start, l_duration);
                     avideo.api_playVideo(l_angle_start, l_duration);
                 }
             }, 2000
             );

                
              //mvideo.api_playVideo(l_master_start, l_duration);
              //avideo.api_playVideo(l_angle_start, l_duration);
            
              break;
            }
          }
       };
    })(vs_myClass, currentIndex);
    
    //add td to tr on timeline table
    add_timeline_tr.appendChild(add_play_angle_td);    
    
    //Edit
    var add_action_td   = document.createElement("td");
    edit_btn = document.createElement("button");
    var t=document.createTextNode("Edit");
    edit_btn.appendChild(t);
    add_action_td.appendChild(edit_btn);
    
    
    del_btn = document.createElement("button");
    var t=document.createTextNode("Delete");
    del_btn.appendChild(t);
    add_action_td.appendChild(del_btn);
    
    
    del_btn.onclick = (function (vs_myClass, currentIndex ){
      return function (){
         if(vs_myClass.debug) console.log("Delete " + currentIndex);
         var table = document.getElementById("video_sync_timeline_table");
          
         for(var i=1; i<timeline_table.rows.length; i++)
         {
           var r = timeline_table.rows[i];
           if (r.index == currentIndex)
           { 
             table.deleteRow(i);
             break;
           }  
         }
      };   
    })(vs_myClass,  currentIndex);    

    
    edit_btn.onclick = (function (vs_myClass, currentIndex ){
      return function (){
          if(vs_myClass.debug) console.log("Edit " + currentIndex);
          var table = document.getElementById("video_sync_timeline_table");
          
          for(var i=1; i<timeline_table.rows.length; i++)
          {
            var r = timeline_table.rows[i];
            if (r.index == currentIndex)
            {
              console.log(r);
              console.log("EDIT THIS SRC: " + r.currentSrc);
              
              vs_myClass.crtIndex = currentIndex; 
              vs_myClass.currentSrc = r.currentSrc;
              mvideo.api_pauseVideo();
              avideo.api_pauseVideo();
              
              var l_master_start = Number(r.cells[0].innerHTML); 
              var l_angle_start  = Number(r.cells[1].innerHTML);
              var l_duration     = Number(r.cells[2].innerHTML); 

              vs_myClass.changeAngleSrc(r.currentSrc, true, l_master_start, l_angle_start, l_duration);

              break;
            }
          }
       };   
    })(vs_myClass,  currentIndex);    
    
    add_timeline_tr.appendChild(add_action_td);   
    timeline_table.appendChild(add_timeline_tr);
    
    vs_myClass.crtIndex = -1;    
    
    
    /*
    var json_obj = new Object();
    json_obj.video_id   = vs_myClass.angle_video.src;
    json_obj.master_start = vs_myClass.master_start;
    json_obj.angle_start  = vs_myClass.angle_start;
    json_obj.duration     = vs_myClass.duration;
    
    //console.log(JSON.stringify(json_obj));
    vs_myClass.timeline_arr.push(json_obj);
    vs_myClass.vs_json = "{ angle: " + JSON.stringify(vs_myClass.timeline_arr) + "}";   
    //console.log(JSON.stringify(vs_myClass.vs_json));
    
    //document.getElementById("sync_json").innerHTML = vs_myClass.vs_json;
    */
    

    
    //IMPORTANT
    /*
     * SAVE THE VALUE FROM vs_myClass.vs_json INTO THE DATABASE AND USE IT ON THE PLAYER
     * 
     */
    
    
};
