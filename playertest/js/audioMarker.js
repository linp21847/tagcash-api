function AudioMarker(audio_marker_main, as_main)
{  
  this.debug = true;
  if(this.debug)
      console.log("AudioMarker constructor: audio_marker_main: " + audio_marker_main );
  
  var me = this;
  
  if(this.debug) 
    console.log("version: " + as_main.api_getPlayerVersion());
  
  this.crt_nr_timeline = 0;
  this.crtIndex = -1;
  
  //SUBMIT SYNC
  this.as_addtotimeline_btn = document.getElementById("audio_marker_addtotimeline"); if(this.debug)  console.log(this.as_addtotimeline_btn);
  this.as_addtotimeline_btn.myClass = this;  
  this.as_addtotimeline_btn.onclick = this.addToTimelineSync;    
    
  //audio
  this.main_audio = as_main;
  //JSON
  this.as_json = {"marker": []};
  this.timeline_arr = [];

  this.master_start = 0;
  this.angle_start  = 0;
  this.duration     = 1;

  $("#audio"+as_main.id).on("loadedmetadata", function()
  {
     if(me.debug) console.log("METADATA LOADED MAIN !!!!!!   duration: "  + as_main.api_getAudioDuration());
     me.masterMetadataLoaded();
  });
  
  this.max_size_marker_text = 75;
}


AudioMarker.prototype.masterMetadataLoaded = function()
{
  console.log("AudioMarker.masterMetadataLoaded");
  var me = this;

  var master_duration = Number(this.main_audio.api_getAudioDuration()).toFixed(2);
 
  $("#audio_marker_slider").slider({
         from: 0, 
         to: master_duration,
         limits: true, 
         step: 0.01, round: 2,
         dimension: '&nbsp;', 
         skin: "round_plastic",
         onstatechange: function( value ){
           var val = Number(value).toFixed(2);
           console.log("Master start " + val);
           me.master_start = val;
           me.main_audio.api_setCurrentTime(val);
           if((master_duration - Number(me.master_start).toFixed(2)) <= Number(me.duration))
           {
               me.duration = master_duration - Number(me.master_start).toFixed(2);
               $("#audio_marker_duration_slider").slider("value", Number(me.duration));
               $("#audio_marker_duration_slider").slider().update();
           }           
         }
   });
        
   $("#audio_marker_slider").slider("value", 0);
   $("#audio_marker_slider").slider().update();
        
   me.master_start = 0;
//new cc   

 
        
  $("#audio_marker_duration_slider").slider({
         from: 0, 
         to: me.getMinDuration(),
         limits: true, 
         step: 0.01, round: 2,
         dimension: '&nbsp;', 
         skin: "round_plastic",
         onstatechange: function( value ){
             var val = Number(value).toFixed(2);
             console.log("val:" + val);
             var max_dur = me.getMaxDuration();
             var curr_duration = val;
             console.log("max_dur:" + max_dur + " curr_duration:" + curr_duration + " me.master_start:" + me.master_start);
  
             if(Number(max_dur) < Number(curr_duration))
             {
                 $("#audio_marker_duration_slider").slider("value", Number(max_dur)); return;

             }                             
             me.duration = val;
             me.main_audio.api_setCurrentTime(Number(val) + Number(me.master_start));
         }          
  }); 
   //me.setSlidersValues(me.master_start,  master_duration);
}

AudioMarker.prototype.getMaxDuration = function()
{
  var max_duration = 0;
  var m_duration = Number(this.main_audio.api_getAudioDuration()).toFixed(2);
  var curr_m_duration =  Number($("#audio_marker_slider").slider("value")).toFixed(2);
  var mas = m_duration - curr_m_duration;
  max_duration = mas;
  
  console.log("getMaxDuration => max_duration: " + Number(max_duration).toFixed(2));
  return Number(max_duration).toFixed(2);
  
}

AudioMarker.prototype.getMinDuration = function()
{
  var m_duration = Number(this.main_audio.api_getAudioDuration()).toFixed(2);
  return m_duration;  
}

AudioMarker.prototype.setSlidersValues = function(master_start,  duration)
{
  console.log("setSlidersValues " + master_start +  " " +  duration);
 
  //set the slider values;
  $("#audio_marker_slider").slider("value", master_start);
  $("#audio_marker_duration_slider").slider("value", duration); 
              
  this.main_audio.api_setCurrentTime(master_start, false);                         
}

AudioMarker.prototype.clearLocalData = function()
{
   this.start_from = 0;
   this.duration = 0;
   this.sync_start = 0;
};

AudioMarker.prototype.getJsonTimeline = function ()
{
  var timeline_arr = [];
  var timeline_table = document.getElementById("audio_marker_timeline_table");
          
  for(var i=1; i<timeline_table.rows.length; i++)
  {
    var r = timeline_table.rows[i];
    //console.log(r);
    
    var json_obj = new Object();
    json_obj.audio_id     = r.currentSrc;
    json_obj.marker_start = Number(r.cells[0].innerHTML);;
    json_obj.marker_duration     = Number(r.cells[1].innerHTML);
    
    if(r.cells[3].childNodes[0].value == "Link")
      json_obj.marker_type = "link";
    else
      json_obj.marker_type  = "text";
    
    json_obj.marker_text = r.cells[3].childNodes[1].value;    
    timeline_arr.push(json_obj);
  }  
  
  var json = { 
    marker: timeline_arr
  };   
  
  if(this.debug)  console.log(JSON.stringify(json));            
  
  return json;
}



AudioMarker.prototype.addToTimelineSync = function ()
{
    var as_myClass = this.myClass;

    if(as_myClass.debug) console.log("Master Start: " + as_myClass.master_start + " duration: " + as_myClass.duration);
    
    var action = "";
    if (as_myClass.crtIndex == -1) //add new
    {
      var currentIndex = ++as_myClass.crt_nr_timeline;    
      action = "add";
    }
    else  //update editing index
    {
      //console.log("Update index " + as_myClass.crtIndex);
      action = "update";
    }
   
    var timeline_table = document.getElementById("audio_marker_timeline_table");

    if (action == "update")
    {
      for(var i=1; i<timeline_table.rows.length; i++)
      {
        var r = timeline_table.rows[i];
        //console.log(r.cells[0].innerHTML);
        if (r.index == as_myClass.crtIndex)
        {
          r.cells[0].innerHTML = $("#audio_marker_slider").slider("value");
          r.cells[1].innerHTML = $("#audio_marker_duration_slider").slider("value");
        }
      }
           
      as_myClass.crtIndex = -1;
      return;
    }
    
    
    
    var add_timeline_tr = timeline_table.insertRow(0);

    add_timeline_tr.main_audio = as_myClass.main_audio; //keep the reference to angle video
    add_timeline_tr.index = currentIndex;
    add_timeline_tr.currentSrc = as_myClass.currentSrc;
    
    var mvideo = as_myClass.main_audio;
    var play_duration = as_myClass.duration;
    var master_start  = as_myClass.master_start; 

    var add_nr_td = document.createElement("td");   
    add_nr_td.innerHTML = currentIndex;    
    //add_timeline_tr.appendChild(add_nr_td);
    
    var add_start_td        = document.createElement("td");
    add_start_td.style.textAlign = "right";  
    add_start_td.innerHTML  = Number(as_myClass.master_start).toFixed(2);
    add_timeline_tr.appendChild(add_start_td);


    var add_duration_td       = document.createElement("td");    
    add_duration_td.style.textAlign = "right";  
    add_duration_td.innerHTML = Number($("#audio_marker_duration_slider").slider("value")).toFixed(2);//Number(as_myClass.duration).toFixed(0);
    add_timeline_tr.appendChild(add_duration_td);
/*
    var add_end_td       = document.createElement("td");    
    add_end_td.style.textAlign = "right";  
    add_end_td.innerHTML = Number(Number($("#audio_marker_duration_slider").slider("value")).toFixed(2) + Number(as_myClass.master_start).toFixed(2)) ;//Number(as_myClass.duration).toFixed(0);
    add_timeline_tr.appendChild(add_end_td);
*/    
    //Play
    var add_play_angle_td = document.createElement("td");
    add_play_angle_td.innerHTML = "<img src='img/play.png' alt='Play' height='24' width='24'>";//"Play";
    add_play_angle_td.onclick = (function (as_myClass, currentIndex ){
      return function (){          
          if(as_myClass.debug) console.log("Play idx " + currentIndex);
          
          var table = document.getElementById("audio_marker_timeline_table");
          for(var i=1; i<timeline_table.rows.length; i++)
          {
            var r = timeline_table.rows[i];
            if (r.index == currentIndex)
            {                            
              var l_master_start = Number(r.cells[0].innerHTML); 
              var l_duration     = Number(r.cells[1].innerHTML); 
              var l_end          = Number(r.cells[2].innerHTML) + Number(r.cells[0].innerHTML); 
              
              $("#audio"+as_myClass.main_audio.id).unbind("loadedmetadata");
             
              mvideo.api_pauseAudio();              
              mvideo.api_setCurrentTime(l_master_start);
              as_myClass.currentSrc = r.currentSrc; //cc            
              mvideo.api_playAudio(l_master_start, l_duration);
              
              //uncomment folowing code and comment the line above if you want to be sure that the clip will
              //play right away. The code check that the marker will be 100% playable
              /*
              var setPlayTimer = setInterval(function()
               {
                 if(as_myClass.main_audio.api_canPlayWithoutDelay() == true)
                 {
                     clearInterval(setPlayTimer);
                     //now both clips will play in sync
                     mvideo.api_playVideo(l_master_start, l_duration);
                     //avideo.api_playVideo(l_angle_start, l_duration);
                 }
               }, 2000
              );
              */
              break;
            }
          }
       };
    })(as_myClass, currentIndex);
    
    //add td to tr on timeline table
    add_timeline_tr.appendChild(add_play_angle_td);    

    //Marker text
    var action_td        = document.createElement("td");
    action_td.style.textAlign = "left";  
    
    var select_marker_type = document.createElement("select");
    select_marker_type.name = "marker_select";
    
    var select_marker_option_link = document.createElement("option");
    select_marker_option_link.text = "Link";
    select_marker_option_link.value = "Link";
    //select_marker_type.add("Link");
    var select_marker_option_text = document.createElement("option");
    select_marker_option_text.text = "Text";
    select_marker_option_text.value = "Text";
    //select_marker_type.add("Text");
    
    var marker_text = document.createElement("input");
    marker_text.type = "text";
    marker_text.name = "marker_text";
    marker_text.id = "marker_text";    
    marker_text.size = as_myClass.max_size_marker_text;    
    marker_text.maxlength = as_myClass.max_size_marker_text;    
    
    select_marker_type.add(select_marker_option_link);
    select_marker_type.add(select_marker_option_text);
    
    action_td.appendChild(select_marker_type);
    action_td.appendChild(marker_text);
    
    add_timeline_tr.appendChild(action_td);

    
    //Edit
    var add_action_td   = document.createElement("td");
    edit_btn = document.createElement("button");
    var t=document.createTextNode("Edit");
    edit_btn.appendChild(t);
    add_action_td.appendChild(edit_btn);
    
    
    del_btn = document.createElement("button");
    var t=document.createTextNode("Delete");
    del_btn.appendChild(t);
    add_action_td.appendChild(del_btn);
    
    
    del_btn.onclick = (function (as_myClass, currentIndex ){
      return function (){
         if(as_myClass.debug) console.log("Delete " + currentIndex);
         var table = document.getElementById("audio_marker_timeline_table");
          
         for(var i=1; i<timeline_table.rows.length; i++)
         {
           var r = timeline_table.rows[i];
           if (r.index == currentIndex)
           { 
             table.deleteRow(i);
             break;
           }  
         }
      };   
    })(as_myClass,  currentIndex);    

    
    edit_btn.onclick = (function (as_myClass, currentIndex ){
      return function (){
          if(as_myClass.debug) console.log("Edit " + currentIndex);
          var table = document.getElementById("audio_marker_timeline_table");
          
          for(var i=1; i<timeline_table.rows.length; i++)
          {
            var r = timeline_table.rows[i];
            if (r.index == currentIndex)
            {
              //console.log(r);
              //console.log("EDIT THIS SRC: " + r.currentSrc);
              
              as_myClass.crtIndex = currentIndex; 
              //as_myClass.currentSrc = r.currentSrc; 
              mvideo.api_pauseAudio();
              
              var l_master_start = Number(r.cells[0].innerHTML); 
              var l_duration     = Number(r.cells[1].innerHTML); 
              
              as_myClass.setSlidersValues(l_master_start, l_duration);
              break;
            }
          }
       };   
    })(as_myClass,  currentIndex);    
    
    add_timeline_tr.appendChild(add_action_td);   
    timeline_table.appendChild(add_timeline_tr);
    
    as_myClass.crtIndex = -1;    
        
    
    //IMPORTANT
    /*
     * SAVE THE VALUE FROM as_myClass.as_json INTO THE DATABASE AND USE IT ON THE PLAYER
     * 
     */
    
    
};
