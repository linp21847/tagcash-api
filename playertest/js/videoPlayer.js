//function VideoPlayer(id, src,poster, autoplay, myvideo, show_angles, angle_json, tags_json)
function VideoPlayer(vps)
{
  //console.log(angle_json);
  
  this.debug = true;
  if(this.debug) console.log("videoPlayer constructor");
  this.pluggins = [];
  
  var me = this;
  console.log(this);
  this.video_el = null;//15
  this.id = vps.id;
  this.myHtmlElem = document.getElementById(this.id); //this.myHtmlElem is null

  this.playbtn       = null;
  this.playprevframe = null; 
  this.playnextframe = null;
  this.seekslider    = null; 
  this.curtimetext   = null; 
  this.durtimetext   = null;
  this.mutebtn       = null;
  this.volumeslider  = null; 
  this.fullscreenbtn = null;
  this.full          = null; 
  this.playslow      = null;
  this.bufferedPercent = 0;
  this.vid_canplaythrough = false;
  
  this.width = vps.width;
  this.height = vps.height;
  
  this.angle_json = vps.angle_json; console.log(this.angle_json);
  this.playbackRate = vps.defaultplayback; //1
  this.muted = null;
  this.orig_playbackRate = 1;
  this.src = vps.src; 
  if(vps.poster) this.poster = vps.poster; else this.poster = null;
  
  this.currentTime = 0;
  this.playafterseek = vps.playafterseek;
  this.vid_duration = 0;
  this.currentVolume = 50;

  //construct and initialize the player
  if(this.debug) console.log("VideoPlayer.initialize id=" + this.id);

  if(vps.is3D == true)
  {
	console.log("============== 3D ================");
	videob = document.createElement('div');
  //console.log (videob);
    videob.className = "video_player_box";
	
    videob.id = me.id;
    video_player_el = videob;
  }
  else
  {
    var video_player_el = document.getElementById(me.id);
  }
  
	video_player_el.style.width = this.width+"px";
	video_player_el.style.height = this.height+"px";

  window.videoInitialWidth = this.width;
  window.videoInitialHeight = this.height;
  
  //video element    
  var video_e = document.createElement('video');
 
  //hook for full screen off
  video_e.addEventListener("webkitfullscreenchange", function () {
    if (!document.webkitIsFullScreen)  $(".video_player_box").show();
  }, false);
  video_e.addEventListener("fullscreenchange", function () {
    if (!document.fullscreen) $(".video_player_box").show();
  }, false);
  video_e.addEventListener("mozfullscreenchange", function () {
    if (!document.mozFullScreen) $(".video_player_box").show();
  }, false);
  video_e.addEventListener("msfullscreenchange", function () {
    if (!document.msFullscreenElement) $(".video_player_box").show();
  }, false);
  //end of hook for full screen off

 
  video_e.setAttribute("id", "video" + this.id);    
  video_e.setAttribute("width", this.width);
  video_e.setAttribute("height", this.height);
  video_e.setAttribute("preload", "auto");
  video_e.setAttribute("data-framerate", "24");
  
  if(vps.autoplay == true) video_e.setAttribute("autoplay", true);
  
  video_e.setAttribute("volume", "0.6");
  if (vps.src) video_e.setAttribute("src", vps.src);
  if (vps.poster) video_e.setAttribute("poster", vps.poster);
  video_e.setAttribute("type", "video/mp4") 
  video_e.setAttribute("codecs", "avc1.42E01E, mp4a.40.2");
  video_e.addEventListener("timeupdate",   function() { me.seektimeupdate();}, false); 
  
  video_e.addEventListener("ended", function() {me.videoEnded();}, false);
  video_e.fullScreen = false;
  
  video_e.addEventListener("loadedmetadata", function()
  {
     if(me.debug) console.log("handler = loadedmetadata");
  }, false);
  
  video_e.addEventListener("canplaythrough", function(){
	 //if(me.debug) console.log("handler = canplaythrough");
     me.vid_canplaythrough = true;     
  }, false);
  

  this.video_el = video_e;
  $(video_e).on("progress", function() {me.updateBuffers();});

  //disable right click on video for download
  $(video_e).on('contextmenu',function() { return false; });
  
video_player_el.appendChild(video_e); 
//$("#" + me.id).append(video_e);
  
  var table_cell = ["td_play", "td_fr", "td_ff", "td_slow", "td_bracketology",
                      "td_videogame", "td_seekslider", "td_curtimetext", 
                      "td_volumeslider", "td_volumebtn", "td_fullscreenbtn"];
    
  

  var video_controls_bar = document.createElement('div');
  video_controls_bar.setAttribute("id", "video_controls_bar" + this.id);
  
  video_controls_bar.setAttribute("style", "position: absolute; width: "+this.width+"px; bottom: 0; left:0; right:0;  color: white;  font-size: .8em; width: 100%; min-height: 34px; padding: 0px; background:url(img/bg.png); z-index: 2147483647;");
    
  var controls_table = document.createElement('table'); //create table for controls
  controls_table.setAttribute("id", "controls_table");
  controls_table.setAttribute("border","0");
  controls_table.setAttribute("width","100%");
  controls_table.setAttribute("style","align: center;");
    
  var tr = document.createElement('tr'); //create TR for the controls table
    
  i = 0;
  do
  {
      var td = document.createElement('td');
      if(i == 0) //play button
      {
		  td.style.width = "25px";
		  this.playbtn = document.createElement("button");

		  this.playbtn.setAttribute("id", "playpausebtn" + this.id);
	      this.playbtn.setAttribute("class", "playpausebtn");
		  
          this.playbtn.onclick = function() {me.playPause()};
          
          if(vps.autoplay == true)
            this.playbtn.style.background = "url(img/pause.png)";
          
		  td.appendChild(this.playbtn); //add PLAY/PAUSE button to its TD
		  
	  }
	  else if( i==1) //playprevframe
	  {
          td.style.width = "25px";
		  this.playprevframe = document.createElement("button");
		  this.playprevframe.setAttribute("id", "playprevframe" + this.id);
          this.playprevframe.setAttribute("class", "playprevframe");
          this.playprevframe.onclick = function() {me.playPrevFrame()};
		  td.appendChild(this.playprevframe); //==> add PREV frame button to its td
	  }
	  else if( i==2) //playnextframe
	  {
          td.style.width = "25px";
		  this.playnextframe = document.createElement("button");
		  this.playnextframe.setAttribute("id", "playnextframe" + this.id);
          this.playnextframe.setAttribute("class", "playnextframe");
          this.playnextframe.onclick = function() {me.playNextFrame()};
		  td.appendChild( this.playnextframe); //==> add NEXT frame button to its TD
	  }	  
	  
	  else if( i==3) //slow
	  {
          td.style.width = "25px";
		  this.playslow = document.createElement("button");
		  this.playslow.setAttribute("id", "playslow" + this.id);
          this.playslow.setAttribute("class", "playslow");
          this.playslow.onclick = function() {me.playSlowFrame()};
		  td.appendChild(this.playslow);
	  }		  
	  else if( i==4) //bracketology
	  {	  console.log("my videoooo --- ");
		  if(vps.myvideo)
          {
            this.bracketology = document.createElement("button");
		    this.bracketology.setAttribute("id", "bracketology" + this.id);
            this.bracketology.setAttribute("class", "bracketology");
            this.bracketology.onclick = function() {me.addToBracketology()};
            td.style.width = "25px";
		    td.appendChild( this.bracketology);
          } else {
              /* because IE can't handle empty cells */
              td.innerHTML = "&nbsp;"
              td.style.width = "1px";
          }
      }
	  else if( i==5) //videogame
	  {	  
		  if(vps.myvideo)
          {		  
            this.videogame = document.createElement("button");
		    this.videogame.setAttribute("id", "videogame" + this.id);
            this.videogame.setAttribute("class", "videogame");
            this.videogame.onclick = function() {me.addToVideogame()};
            td.style.width = "25px";
            td.appendChild( this.videogame);
          } else {
              td.innerHTML = "&nbsp;"
              td.style.width = "1px";              
          }
      }      
      else if( i==6) //td_seekslider
	  {
		  this.seekslider = document.createElement("input");
		  this.seekslider.setAttribute("id", "seekslider" + this.id);
          //this.seekslider.style.width = "220px";
		  this.seekslider.setAttribute("class", "seekslider");
		  this.seekslider.setAttribute("type", "range");
		  this.seekslider.setAttribute("min", "0");
		  this.seekslider.setAttribute("max", "100");
		  this.seekslider.setAttribute("value", "0");
		  this.seekslider.setAttribute("step", "any");
		  
		  this.seekslider.addEventListener("change", function() {me.vidSeek()}, false);
		  this.seekslider.addEventListener("input", function() {me.vidSeek()}, false);
          if(this.playafterseek == true)
          {          
		     this.seekslider.addEventListener("mousedown", function(){  
                  me.video_el.pause();
                 }, false);
		     this.seekslider.addEventListener("mouseup", function(){
                  me.video_el.play();                  
                  me.playbtn.style.background = "url(img/pause.png)";
               }, false);
          }               
		  td.appendChild( this.seekslider);
	  }
      else if( i==7) //td_curtimetext
	  {
          td.style.textAlign = "center";
	      this.curtimetext = document.createElement("span");
	      this.curtimetext.setAttribute("id", "curtimetext" + vps.id);
	      this.curtimetext.setAttribute("style", "font-family: Arial; font-size: .9em;");
	      this.curtimetext.innerHTML = "00:00";
	      td.appendChild( this.curtimetext);
	      
	      var delimitator = " / ";
	      td.innerHTML = td.innerHTML + delimitator;
	      
	      this.durtimetext = document.createElement("span");
	      this.durtimetext.setAttribute("id", "durtimetext" + vps.id);
	      this.durtimetext.setAttribute("style", "font-family: Arial; font-size: .9em;");
	      this.durtimetext.innerHTML = "00:00";
	      td.style.width = "80px";
          td.appendChild( this.durtimetext);
      }
      else if( i==9) //td_volumeslider
      {
		  this.volumeslider = document.createElement("input");
		  this.volumeslider.setAttribute("id", "volumeslider" + vps.id);
		  this.volumeslider.setAttribute("class", "volumeslider");
		  this.volumeslider.setAttribute("type", "range");
		  this.volumeslider.setAttribute("min", "0");
		  this.volumeslider.setAttribute("max", "100");
		  this.volumeslider.setAttribute("value", "0");
		  this.volumeslider.setAttribute("step", "any");
		  
		  this.volumeslider.addEventListener("change",function(){me.setVolume()},false);
		  this.volumeslider.addEventListener("input", function(){me.setVolume()} ,false);
		  
          //initial volume
          var initialVolume = 0.6;
          $(this.volumeslider).css('background-image',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + initialVolume + ', #A2A2A2), '
                + 'color-stop(' + initialVolume + ', #222222)  '
                + ')'
                );
          this.volumeslider.value = 0.5 * 100;
       //   video_e.volume = 0;		
            
		  td.style.width = "60px";
          td.appendChild( this.volumeslider);
	  }
	  
	  else if( i==8)  //td_volumebtn
	  {
		  this.mutebtn = document.createElement("button");
		  this.mutebtn.setAttribute("id", "volumebtn" + this.id);
          this.mutebtn.setAttribute("class", "volumebtn");
          this.mutebtn.onclick = function() {me.muteVolume()};
          td.style.width = "25px";
          td.appendChild( this.mutebtn);		  
	  }
      
      else if( i==10)  //td_fullscreenbtn
	  {
		  this.fullscreenbtn = document.createElement("button");
		  this.fullscreenbtn.setAttribute("id", "fullscreenbtn" + this.id);
          this.fullscreenbtn.setAttribute("class", "fullscreenbtn");
          this.fullscreenbtn.onclick = function() {me.toggleFullScreen()};
          td.style.width = "25px";
          td.appendChild( this.fullscreenbtn);		  
	  }
	  //add this td to the tr
      tr.appendChild(td); // add one td at time to tr
  } while(++i < table_cell.length)
    
  controls_table.appendChild(tr); //add tr to table
    
  video_controls_bar.appendChild(controls_table); //append table to the controls div

video_player_el.appendChild(video_controls_bar);    
//$("#" + me.id).append(video_controls_bar);  
   
   
  if (vps.autoplay == true) video_e.play();
  

  if(vps.autoResize) {
	window.onresize = function(event) {
		var h = window.innerHeight 
			   || document.documentElement.clientHeight 
			   || document.getElementsByTagName('body')[0].clientHeight;
		var w = window.innerWidth 
				|| document.documentElement.clientWidth 
				|| document.getElementsByTagName('body')[0].clientWidth;		
		console.log(w,h);
		me.resize(vps,w,h);
	};  
  }
}//end of constructor



//1104 udpdate buffers is called until the video source is 100% downloaded from server 
//can be used for progress
VideoPlayer.prototype.updateBuffers = function()
{
    var vid = this.video_el;
    
    var buffered = vid.buffered;
    if (buffered.length > 0) {
       var width = (buffered.end(buffered.length - 1) - buffered.start(0)) / vid.duration * 100;
       this.bufferedPercent = width;
    }    
    
}
VideoPlayer.prototype.setSrc = function(src) {
    if(this.debug) console.log("VideoPlayer.setSrc=" + src);
    //this.src = src;
    var me_id = this.me_id;
    var vid = document.getElementById("video" + me_id);
    vid.src = src;
    vid.play();    
};




VideoPlayer.prototype.buffered_loaded = function (e)
{
    console.log("buffered_loaded"); console.log(this.id);
    //var vid = document.getElementById(this.id);
    //console.log(vid);
    console.log("Start: " + e.srcElement.buffered.start(0) + " End: "  + e.srcElement.buffered.end(0));

}


VideoPlayer.prototype.changeSrcTo = function(id, video_id, angle_start, duration) {

    console.log("VideoPlayer.changeSrcTo video_id: " + video_id + " angle_start:" + angle_start + " duration:" + duration);
    //var me_id = this.me_id;
	var vid = this.video_el;
    
    this.playbackRate = vid.playbackRate; //save the playbackRate
    
    if(this.debug) console.log("vid.src:" + vid.src);
    
    var index = vid.src.lastIndexOf("/") + 1;
    var vid_file = vid.src.substr(index);
    
    if(vid_file.lastIndexOf("#t") > -1)
    {
        index = vid_file.lastIndexOf("#t");
        vid_file = vid_file.substring(0,index);
    }
    
    if(this.debug)console.log("=>vid_file.src:" + vid_file + " myClass.src:" + this.src );
    
    if(vid_file == this.src)
    {      
      console.log("************changeSrc vid_file=myClass.src: " + this.src);
    }
  
	vid.src = video_id+"#t=" + angle_start;
	
    vid.bufferedPercent = 0;

    vid.load();
    vid.play();//cc
    vid.playbackRate = this.playbackRate;
	 
    //remove existing handler
    //vid.removeEventListener("timeupdate",this.seektimeupdate,false); 
    //vid.removeEventListener("ended", this.videoEnded, false);
    
}

VideoPlayer.prototype.playPrevFrame = function ()
{
    if(this.debug) console.log("playPrevFrame");

    var vid = this.video_el;
    if(vid.paused)
    {
       vid.currentTime = vid.currentTime - 1/24; 
    }
};

VideoPlayer.prototype.playNextFrame = function ()
{
    if(this.debug) console.log("playNextFrame");
    
    var vid = this.video_el;
    if(vid.paused)
    {
       vid.currentTime = vid.currentTime + 1/24; 
    }
};



VideoPlayer.prototype.vidSeek = function ()
{
	//if(this.debug) console.log("vidSeek");
    var vid = this.video_el;
	var seekto = vid.duration * (this.seekslider.value / 100);
	vid.currentTime = seekto;
};


VideoPlayer.prototype.seektimeupdate = function ()
{
    var vid = this.video_el;
	//console.log("seektimeupdate " + this.id + " " + vid.currentTime);
	
    var nt = vid.currentTime * (100 / vid.duration);
	this.seekslider.value = nt; // 0-100% percent value
	var curmins = Math.floor(vid.currentTime / 60);
	var cursecs = Math.floor(vid.currentTime - curmins * 60);
	var durmins = Math.floor(vid.duration / 60);
	var dursecs = Math.floor(vid.duration - durmins * 60);
	if(cursecs < 10){ cursecs = "0"+cursecs; }
	if(dursecs < 10){ dursecs = "0"+dursecs; }
	if(curmins < 10){ curmins = "0"+curmins; }
	if(durmins < 10){ durmins = "0"+durmins; }
	
    curtimetext = document.getElementById("curtimetext" + this.id);
    curtimetext.innerHTML = curmins + ":" + cursecs;
    durtimetext = document.getElementById("durtimetext" + this.id);
    durtimetext.innerHTML = durmins + ":" + dursecs;
    
    var val = nt/100;

    var w = this.seekslider.parentNode.offsetWidth;
    var val2 = nt * w / 100;// + 1;
    var val3 = this.bufferedPercent * w/100;
    this.seekslider.setAttribute("style", 
                       "background-color: #000000; " + 
                       "background-image: url(img/played.png), url(img/buffered.png);" + 
                       "background-position: left top, left top; " + 
                       "background-repeat: no-repeat; " + 
                       "background-size: " + val2 + "px 7px, " + val3 + "px 7px; "
                      );
};

VideoPlayer.prototype.playSlowFrame = function ()
{
    if(this.debug) console.log("playSlow button");
    var vid = this.video_el;
      
    if(vid.playbackRate == 1)
    {
        vid.playbackRate = 0.5;
        this.playslow.style.background = "url(img/1x4.png)";
        vid.muted = true;        
    }
    else if(vid.playbackRate == 0.5)
    {
        vid.playbackRate = 0.25;
        this.playslow.style.background = "url(img/1x1.png)";
        vid.muted = true;
    }
    else if(vid.playbackRate == 0.25)
    {
        this.playslow.style.background = "url(img/1x2.png)";
        vid.playbackRate = 1;
        vid.muted = false;
    }
    
    if(vid.paused)
    {
        vid.play();
		this.playbtn.style.background = "url(img/pause.png)";
        this.playprevframe.disabled = "disabled";
        this.playnextframe.disabled = "disabled";        
    }    
};


VideoPlayer.prototype.keyPressed = function(e){
  console.log("keyPressed");
}


VideoPlayer.prototype.toggleFullScreen = function(e){
    
    var vid = this.video_el;
    console.log("toggleFullScreen " + this.id + " fullscreen:" + vid.videoIsFullScreen);
    
    $(".video_player_box").hide();   
    $(this.myHtmlElem).show();

	if(vid.requestFullScreen){
		vid.requestFullScreen(); 
	} else if(vid.webkitRequestFullScreen){	
        vid.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
	} else if(vid.mozRequestFullScreen){
		vid.mozRequestFullScreen(); 
	}
    
    var state1 = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
    var event1 = state1 ? 'FullscreenOff' : 'FullscreenOn';
    console.log(event1);
    if(event1 == "FullscreenOff")
    {
        if(vid.webkitExitFullScreen) {vid.webkitExitFullScreen(); }
        else if(vid.mozCancelFullscreen) {vid.mozCancelFullscreen();}
        else if(vid.exitFullscreen) {vid.exitFullscreen();}
        $(".video_player_box").show();
    }
    else if(event1 == "FullscreenOn")
    {

    }   
}

VideoPlayer.prototype.setVolume = function()
{               
  var vid = this.video_el;
  var volumeslider = this.volumeslider;
  vid.volume = this.volumeslider.value / 100;
  
  $(volumeslider).css('background-image',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + vid.volume + ', #A2A2A2), '
                + 'color-stop(' + vid.volume + ', #222222)'
                + ')'
  );                
};

VideoPlayer.prototype.muteVolume = function()
{
	var vid = this.video_el;
    console.log("volume", vid.volume);
   	if(vid.muted){
		vid.muted = false;
        this.mutebtn.style.background = "url(img/vol.png)";
        this.volumeslider.value = vid.volume*100;
        if(this.debug) console.log("Mute action. Current volume=" + vid.volume);
	} else {
		vid.muted = true;
        this.mutebtn.style.background = "url(img/mute.png)";
        this.volumeslider.value = 0;
	}
   
};

VideoPlayer.prototype.videoEnded = function() 
{ 
    if(this.debug) console.log("vid ended");
    this.playbtn.style.background = "url(img/play.png)";
    
    var vid = this.video_el; 
    vid.currentTime = 0;

    vid.poster = this.poster;
};

VideoPlayer.prototype.videoPause = function(){    
    
    if(this.debug) console.log("VideoPlayer.videoPause id=" + this.id); 
    
	var vid = this.video_el;

    if(vid.paused)
      return;
    else
    {
		if(this.debug) console.log("play id=" +  this.id);
        vid.play();
		this.playbtn.style.background = "url(img/pause.png)";
        this.playprevframe.disabled = "disabled";
        this.playnextframe.disabled = "disabled";
	} 
}	

VideoPlayer.prototype.playPause = function(){    
    
    if(this.debug) console.log("VideoPlayer.playPause id=" + this.id); 
    
	var vid = this.video_el;

    if(vid.paused){
		if(this.debug) console.log("play id=" +  this.id);
        vid.play();
		this.playbtn.style.background = "url(img/pause.png)";
        this.playprevframe.disabled = "disabled";
        this.playnextframe.disabled = "disabled";
	} 
    else 
    {
		if(this.debug) console.log("paused id=" +  this.id);
        vid.pause();
		this.playbtn.style.background = "url(img/play.png)";
        this.playprevframe.disabled = "";
        this.playnextframe.disabled = "";
	}
    
    /*
     * due to a bug in mozilla's range input interpretation, we need this hack 
     */
    if ( !(window.mozInnerScreenX == null) ) { // this means the browser is firefox
        $(".seekslider").css("background", "none")
    }

};

VideoPlayer.prototype.resize = function(vps, width, height) {

	height = height - 32; // because of the controls bar
	
	/* we dont want it to be bigger than initial setup */
	width = Math.min(window.videoInitialWidth, width);
	height = Math.min(window.videoInitialHeight, height);

	width = Math.max(vps.minimumWidth, width);
	height = Math.max(vps.minimumHeight, height);
	
	
	var vid = this.video_el;
	console.log("vid",vid);
	
	console.log("w1",this);
	vid.width = width;
	vid.height = width;
	
	//document.getElementById("video_controls_bar" + this.id).style.width = width+"px";
	document.getElementById(this.id).style.width = width+"px";
	document.getElementById(this.id).style.height = height+"px";

	document.getElementById("video"+this.id).style.width = width+"px";
	document.getElementById("video"+this.id).style.height = height+"px";
	

	
	console.log("w2",this.width);
	console.log("vid ww",vid.width);
	console.log("video_controls_bar" + this.id);
	return true;
}




//========================= EXTERNAL ACTIONS
VideoPlayer.prototype.addToBracketology = function ()
{
	if(this.debug) console.log("addToBracketology");
	alert("Added to bracketology");
};

VideoPlayer.prototype.addToVideogame = function ()
{
	if(this.debug) console.log("addToVideogame");
	alert("Added to Videogame");
};


//========================= API FUNCTIONS =====================================

VideoPlayer.prototype.api_getPlayerVersion = function() {
    return "3.0";
};

//api call for getting the status 
//"canplaythrough" event occurs when the browser estimates it can play through the specified audio/video without having to stop for buffering
VideoPlayer.prototype.api_canPlayWithoutDelay = function() 
{	
	if(this.debug) 
      console.log("VideoPlayer.api_canPlayWithoutDelay return: " + this.vid_canplaythrough);
	return this.vid_canplaythrough;
};
VideoPlayer.prototype.api_setCurrentTime = function(crt_time, play_video) {    
    console.log("VideoPlayer.api_setCurrentTime to: "+ crt_time);
    
    var me = this;
    this.video_el.addEventListener("canplay",function() {         
        me.video_el.currentTime = crt_time;
        if(play_video == true) me.video_el.play();
        //remove myself after trigger
        me.video_el.removeEventListener("canplay", arguments.callee, false);
    }, false);

    me.video_el.currentTime = crt_time;
};

VideoPlayer.prototype.api_setCurrentTimeSrc = function(crt_time, play_video) {    
    if(this.debug) 
       console.log("VideoPlayer.api_setCurrentTimeSrc to: "+ crt_time);
    
    var me = this;    
    this.video_el.addEventListener("canplay",function() {         
        me.video_el.currentTime = crt_time; //orig
        //me.video_el.src = me.video_el.src + "#t=" + crt_time;
        if(play_video == true)
          me.video_el.play();
        //remove myself after trigger
        me.video_el.removeEventListener("canplay", arguments.callee, false);
        }, false);
    //me.video_el.currentTime = crt_time;
};


VideoPlayer.prototype.api_addListener = function(videoListener, function_name) {
	if(me.debug) console.log("api_addListener for: " + videoListener);
    this.video_el.addEventListener(videoListener, function_name, false);
};


VideoPlayer.prototype.api_removeListener = function(videoListener, function_name) {
	if(me.debug) console.log("api_removeListener for: " + videoListener);
    this.video_el.removeEventListener(videoListener, function_name, false);
};

VideoPlayer.prototype.api_playVideo = function(startFrom, duration) {  
    //startFrom == 0 and duration == -1 play entire video
    console.log("api_playVideo startFrom:" + startFrom + " for duration: " + duration);
    var vid = this.video_el;
    
    var b = (function(vid, startFrom, duration){
		return function () {
		    if(vid.currentTime >= (startFrom + duration))
		     vid.pause();		    
	      }
		})(vid, startFrom, duration);
    
    if((duration > 0 ) && (startFrom >=0) )
    {
       console.log("api_playVideo unbind and bin timeupdate for: " + this.video_el.id);
       //$("#" + this.video_el.id).unbind("timeupdate");
       //$("#" + this.video_el.id).bind("timeupdate", b );

       $(this.video_el).off("timeupdate");
       $(this.video_el).on("timeupdate", b );       
    }
    
    vid.play();
};

VideoPlayer.prototype.api_pauseVideo = function() {  
   this.video_el.pause();
};

VideoPlayer.prototype.api_getCurrentTime = function()
{
   var crtTime = this.video_el.currentTime;
   //if(this.debug) console.log("VideoPlayer.api_getCurrentTime:" + crtTime);
   return crtTime; 
};

VideoPlayer.prototype.api_changeVideoSrc = function(new_src) {  
    this.vid_canplaythrough = false; //this will be se to true by the "canplaythrough" event
    this.video_el.src = new_src;  
    this.video_el.load();
};

VideoPlayer.prototype.api_getVideoDuration = function() { 
  if (this.video_el) return this.video_el.duration;
  return null;  
}


VideoPlayer.prototype.api_addPluggin = function(p) { 
  console.log("api_addPluggin");
  p.createHtml(this);
  p.hookIt(this);
  
  this.pluggins.push(p);
}

VideoPlayer.prototype.api_reloadPlugginMetadata = function() { 
  console.log("api_reloadPlugginMetadata");

  for(var i=0; i<this.pluggins.length; i++) 
  {
    this.pluggins[i].reloadMetadata(this);
  } 
}


VideoPlayer.prototype.api_savePlugginsSnapshot = function() { 
  console.log("api_savePlugginsSnapshot");

  for(var i=0; i<this.pluggins.length; i++) 
  {
    this.pluggins[i].saveSnapshot(this);
  } 
}


VideoPlayer.prototype.api_restorePlugginsSnapshot = function() { 
  console.log("api_restorePlugginsSnapshot");

  for(var i=0; i<this.pluggins.length; i++) 
  {
    this.pluggins[i].restoreSnapshot(this);
  } 
}


window.requestAnimFrame = (function(){
      return  window.requestAnimationFrame       || 
              window.webkitRequestAnimationFrame || 
              window.mozRequestAnimationFrame    || 
              window.oRequestAnimationFrame      || 
              window.msRequestAnimationFrame     || 
              function(callback,element){
                  console.log(element);
                window.setTimeout(callback, 1000 / 60);
              };
})();
