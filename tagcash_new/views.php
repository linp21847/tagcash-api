<?php
function httpget($url){
    if(is_callable('curl_init')) {
        $ch= curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result= curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result, true);
        return $json;
    } else {
        return false;
    }
}
function downloadfile( $url ){
    //ob_start();
    $ch = curl_init( $url );
    $filename = basename( $url );
    $fp = fopen( $filename , 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
    //ob_get_clean();
    return $filename;
}

$user_id = '1486';
$media_id = '899';
$tooltip_array = array();
$out= httpget('http://front-api.tagcash.tv/sharing/media/'.$media_id.'/user/'.$user_id);
if ( $out ) {
    $tooltip_array  = $out['data'] ;
}

$index = 0;
foreach ($tooltip_array['hotspot_item'] as  $product) {
    foreach ($product['tagging_items'] as $hotspot ) {
        $index++;
        
    }   
}

echo "<pre>";print_r($out);echo "</pre>";
?>
<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <meta charset="utf-8">
        <title>TagCash</title>
        <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="view_header">
            <div class="row">
                <div class="view_logo">
                    <a href="index.html">
                    <img src="images/201401/logo.png" alt="" />
                    </a>
                </div>
            </div>
            </div><!--header end-->

            <div id="content" class="pad_top">
                <div id="hotspot_page">

                    <div class="pro1">
                        <?php 
                        $myImgOriginal = str_replace("http://","https://",$tooltip_array['thumb_url_orignal']);
                        $filename = downloadfile( $myImgOriginal );
                        $imgInfo    = getimagesize($filename);
                        @unlink( $filename );
                        $scale      = 1;
                        if($imgInfo[0] > 570){
                            $scale  = 570/$imgInfo[0];
                        }
                        $width      = $imgInfo[0]*$scale;
                        $height     = $imgInfo[1]*$scale;

        
                        foreach ($tooltip_array['hotspot_item'] as  $hotspot) {


                            foreach ($hotspot['tagging_items'] as $key => $tagelement) {

                                $coor_x1    = $hotspot['coor_x1']*$width + 40;
                                $coor_y1    = $hotspot['coor_y1']*$height + 18;
                        ?>
                        
                        <div class="tool">

                            <div class="tol_im2" style="left:<?php echo $coor_x1; ?>px;top:<?php echo $coor_y1; ?>px;">
                                <a href=""  class="boxi_hov"><img src="images/tag_icon.png"/>
                                <span class="tp_box tp_small">
                                <div class="padd_box">

                                    <div class="toltip_ima">
                                        <img src="<?php echo $tagelement['imageurl'] ?>"/>
                                    </div>

                                    <div class="item_info">
                                       <p class="item_store"><?php echo $tagelement['merchant'] ?></p>
                                        <p class="item_name"><?php echo $tagelement['name'] ?></p>
                                        <p class="item_price"><?php echo $tagelement['price'] ?> USD</p>
                                        <p class="item_btn_buy">&nbsp; </p>
                                        <div class="clr"></div>
                                    </div>
                                </div>
                                </span>
                                </a>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="item_pag">
                            <ul>
                                <li rel="2301" id="page-item" class="peritem">1</li>
                                <li rel="2303" id="page-item" class="peritem">2</li>
                                <li rel="2302" id="page-item" class="peritem active">3</li>
                            </ul>
                        </div>
                    <?php } ?>
                        <img src="<?php echo $tooltip_array['thumb_url_orignal'];?>" alt="tagcash user">
                    </div>

                    <div id="media_info">
                        <div id="who_when_pic">
                            <img class="user_avatar" src="<?php echo $tooltip_array['thumb_avatar'];?>" alt="tagcash user">
                            <div id="who_when">
                                <div id="who"><?php echo $tooltip_array['name'];?></div>
                                <div id="when"><?php echo $tooltip_array['duration'];?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="title"><?php echo $tooltip_array['content'];?></div>
                        <div id="like_comm_tags">
                            <?php echo $tooltip_array['total_like'];?>&nbsp;Likes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $tooltip_array['total_comment'];?>&nbsp;Comment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo count($tooltip_array['hotspot_item']);?>&nbsp;Tags
                        </div>
                    </div>
                    </div><!--main part end-->

                    <div class="main_but">

                        <div id="downloadtheapp_call2action">
                            Download the App
                        </div>


                        <div id="gettheapp_buttons">
                            <a href="https://itunes.apple.com/us/app/tagcash/id718605786?mt=8" target="_blank" title="Download Tagcash on AppStore">
                            <div id="gettheapp_button_mac" class="gettheapp_button">
                            </div>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=tv.tagcash" target="_blank" title="Download Tagcash on GooglePlay">
                            <div id="gettheapp_button_android" class="gettheapp_button">
                            </div>
                            </a>
                        </div>

                    </div>


                </div>
            </body>
       </html>
        
        