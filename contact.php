<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xml:lang="en" lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>  
<link href="images/favicon.ico" rel="icon" type="image/ico" />	
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>TagCash</title>	
        
<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/jquery.fancybox-1.3.4.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/tipTip.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/jquery.bxslider.css" media="screen" rel="stylesheet" type="text/css" />	
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

<link href="css/responsive.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="js/function.js"></script>
<script type="text/javascript" src="js/jquery.tipTip.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script>		
		<script>
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		 })(window,document,'script','../www.google-analytics.com/analytics.js','ga');
		
		 ga('create', 'UA-41514947-1', 'tagcash.tv');
		 ga('send', 'pageview');
		
		</script> 
  <script src="//code.jquery.com/jquery-1.9.1.js"></script>
 
 <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>   
  <script>
  
  // When the browser is ready...
  $(function() {
  
    // Setup form validation on the #register-form element
    $("#contactFrm").validate({
    
        // Specify the validation rules
        rules: {
            uname: "required",
            email: {
                required: true,
                email: true
            },
			comment: "required"
        },
        // Specify the validation error messages
        messages: {
            uname: "Please enter your name",
            email: "Please enter a valid email address",
			comment: "Please enter your message"
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  </script>
<style type="text/css">
.error{color:#F00;font-size:12px;}
</style>

 </head>
<body><?php 
if(isset($_POST['submit'])){
    $to = $_POST['email']; 
    $uname = $_POST['uname'];
    $phone = $_POST['phone'];
	$company = $_POST['company'];
	$comment = $_POST['comment'];
    $subject = 'TagCash';
    $message = "Name:-" . $uname . "\n";
	if($phone){
		$message .= "Phone:-" . $phone . "\n"; 
	}
	if($company){
		$message .= "Company:-" . $company . "\n";
	}
	$message .= "Comment:-" . $comment . "\n";
    $headers = "From: akshat.sequester@gmail.com" . "\r\n" .
				"CC: akshat.sequester@gmail.com";

    mail($to,$subject,$message,$headers);
	echo "<script>alert('Mail Sent. Thank you!');</script>";
}
?>
				
		<!-- PAGE CONTENT -->
		<div id="content_wrap" class="row_wrap">
			<div id="content" class="row">
					<div class="page">
	   	<div class="page-header">SEND US A MESSAGE</div>
      	<div class="login-colum max_width">
        <div id="messageResponse"></div>
	      	<form name="contactFrm" id="contactFrm" method="post" action="#">
	         	<div class="name"></div>
	           	<div class="elem">
	            	<label for="uname">Name:</label>
	               	<input type="text" name="uname" id="uname" value="" class="text">	               	<p class="frm_error"></p>
                   
	         	</div>
	         	<div class="elem">
	            	<label>Phone Number:</label>
	               	<input type="text" name="phone" id="phone" value="" class="text">	               	<p class="frm_error"></p>
	         		
                </div>
	         	<div class="elem">
	            	<label for="email">E-Mail:</label>
	               	<input type="text" name="email" id="email" value="" class="text">	               	<p class="frm_error"></p>
	         		
                </div>
	         	<div class="elem">
	            	<label>Company:</label>
	               	<input type="text" name="company" id="company" value="" class="text">	               	<p class="frm_error"></p>
	         	</div>
	         	<div class="elem">
	            	<label class="message">Message:</label>
	               	<textarea name="comment" id="comment" class="textarea" rows="24" cols="80"></textarea>	               	<p class="frm_error"></p>
	         		
                </div>
	           	<div class="elem">
                <label class="blank_lab">&nbsp; </label>
	            	<p class="new_la">
                    <input type="submit" id="btn-submit" class="button floatright" name="submit" onclick="return validate();" value="Submit"/>
                    <!--<a href="#" id="btn-submit" class="button floatright">SUBMIT</a>--></p>
	         	</div>
				<div class="clear"></div>
	        </form> 	
     	</div>
  	</div>
  	
<script>
	$(document).ready(function(){
		$('#btn-submit').click(function(){
			var options = { 
					dataType: "json", 
					success	: optionsEnt
				}; 
			$('#frm-contact').ajaxSubmit(options);
			$('.global_loading').show();
			return false;
		});
		function optionsEnt(data){
			$('.global_loading').hide();
			$('.frm_error').hide();
			if(data.type == 'success'){
				popupAlert('success', data.msg);
				$('#frm-contact')[0].reset();
			}else{	
				for(var x in data.msg){
					var id = $('#'+x);
					loadError(id,data.msg[x]);
				}
			}
		}
	})
</script>			</div>
		</div>
       
</body>

<!-- Mirrored from tagcash.tv/index/contact by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 13 Nov 2014 07:50:18 GMT -->
</html>