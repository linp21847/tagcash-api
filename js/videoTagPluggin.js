function VideoTagPluggin(video_tags_json, tagClickCallBack)
{
    console.log("VideoTagPluggin.constructor");

    this.video_tags = this.convertJsonTags(video_tags_json);
    this.snapshot = null;

    this.tag_canvas = null;
    this.ctx = null;

    this.imgTag = new Image();
    this.imgTag.src = "img/TagCash_tag.24x24.png";
    this.imgTag.id = "base_tag";
    this.imgTag.style.width = "24px";
    this.imgTag.style.height = "24px";

    this.tagsVisible = false;

    this.tagClickCallBack = tagClickCallBack;
}

VideoTagPluggin.prototype.setVideoTags = function(video_tags_json)
{
    console.log("VideoTagPluggin.setVideoTags");
    this.video_tags = this.convertJsonTags(video_tags_json);
}

VideoTagPluggin.prototype.convertJsonTags = function(json)
{
    console.log("VideoTagPluggin.convertJsonTags");

    /*video_tags = [
     { time: 1, visible: 1, x1: 11, y1: 10 }
     { time: 5, visible: 1, x1: 18, y1: 40 }
     ] 
     => 
     var tag_json1 = [
     {tag_id: "tag1",  x0: 0,  y0: 5,  x1: 340,   y1:  130,  t0: 2,   t1: 10},
     ] */

    var res_tag = []

    for (var i = 0; i < json.length; i++)
    {
        var tag = json[i];
        for (var j = 1; j < tag.path.length; j++)
        {
            var end1 = tag.path[j - 1];
            var end2 = tag.path[j];

            if (Number(end1.visible) == 1)
            {
                res_tag.push({
                    tag_id: tag.tag_id,
                    x0: end1.x,
                    y0: end1.y,
                    t0: end1.time,
                    x1: end2.x,
                    y1: end2.y,
                    t1: end2.time,
                    name: tag.name,
                    merchant: tag.merchant,
					price: tag.price,
                    url: tag.url,
                    image: tag.image,
                    detail_visibility: tag.detail_visibility
                });
            }
        }
    }


    return res_tag;
}

VideoTagPluggin.prototype.createHtml = function(vp)
{
    console.log("VideoTagPluggin.createHtml");
    var me = this;

    this.tag_canvas = document.createElement("canvas");
    this.ctx = this.tag_canvas.getContext("2d");
    this.tag_canvas.setAttribute("id", "tag_canvas_" + vp.id);
    this.tag_canvas.setAttribute("width", vp.width);
    this.tag_canvas.setAttribute("height", vp.height);
    this.tag_canvas.setAttribute("style", " position: absolute; top:0px; left:0px; " +
            " z-index: 100; "); //border:1px solid red;");


    this.tag_canvas.onclick = function(e) {
        me.onCanvasClick(vp, e)
    };



    vp.myHtmlElem.appendChild(this.tag_canvas);
}

VideoTagPluggin.prototype.hookIt = function(vp)
{
    console.log("VideoTagPluggin.hookIt");

    var me = this;
    vp.video_el.addEventListener("timeupdate", function() {
        me.timeupdateTags(vp)
    }, false);

}

VideoTagPluggin.prototype.onCanvasClick = function(vp, e)
{
    var x = e.layerX;
    var y = e.layerY;
    var crtTime = vp.api_getCurrentTime();

    console.log("canvasClick " + x + " " + y + " " + crtTime);
    var me = this;

    if (!this.tagsVisible)  //was invisible
    {
        this.tagsVisible = true;
        this.drawTags(vp);
        return;
    }

	/* actual video size may be different than canvas size (vertical videos for example) */
	var left_margin = (vp.width - vp.actualWidth)/2;
	var top_margin = (vp.height - vp.actualHeight)/2;	

    for (var i = 0; i < this.video_tags.length; i++)
    {
        var tag = this.video_tags[i];
        //console.log(tag);

        if (Number(crtTime) < tag.t0)
            continue;
        if (Number(crtTime) > tag.t1)
            continue;

        //interval match
        var delta_x = (tag.x1 - tag.x0) * (crtTime - tag.t0) / (tag.t1 - tag.t0) + left_margin;
        var delta_y = (tag.y1 - tag.y0) * (crtTime - tag.t0) / (tag.t1 - tag.t0) + top_margin;

        tag_x = tag.x0*vp.width + delta_x;
        tag_y = tag.y0*vp.height + delta_y;

		var click_margin = 20;
		
        if ((x > tag_x - click_margin) && (x < tag_x + click_margin) && (y > tag_y - click_margin) && (y < tag_y + click_margin))
        {
            console.log("*------------------------------");
            console.log("Click on " + tag.tag_id);
			
			console.log("#img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1);
			$("img_tag_detail_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1).hide();
			
			document.getElementById("img_tag_detail_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1).style.display = (document.getElementById("img_tag_detail_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1).style.display == "none")?"":"none";
            
            tag.detail_visibility = (tag.detail_visibility)?false:true;

            if (this.tagClickCallBack) {
                this.tagClickCallBack(tag.tag_id);
            } else { 
                alert("Click on " + tag.tag_id);
            }
            return;
        }
    } //end for i   

    for (var i = 0; i < this.video_tags.length; i++) {
        tag = this.video_tags[i];
		if(document.getElementById("img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1)) {
			document.getElementById("img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1).style.display = "none";
		}
    }

    console.log("Click on no tag");
    this.tagsVisible = false;
    this.ctx.clearRect(0, 0, vp.width, vp.height);
}



VideoTagPluggin.prototype.timeupdateTags = function(vp)
{
    var vid = vp.video_el;
    var currTime = vid.currentTime;
    var me = this;

    //console.log("timeupdateTags " + currTime);

    if (!this.tagsVisible)
        return;

    if (currTime == 0)
        return;
    requestAnimFrame(function() {
        me.drawTags(vp)
    });
}

VideoTagPluggin.prototype.drawTags = function(vp)
{
    if (!this.tagsVisible)
        return;

    var vid = vp.video_el;
    var currTime = vid.currentTime;
    //console.log("drawTags " + vp.id + " " + currTime);
    
    var me = this;
	
	function move(x, y, tag) {
		document.getElementById("img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1).style.left = (x-12)+"px"
		document.getElementById("img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1).style.top = (y-12)+"px"
	}

    function draw(x, y, tag) {

		//640, 330

//        me.ctx.drawImage(me.imgTag, x - 12, y - 12, 24, 24);//, 24, 24);
		
		var tag_html = '<div>';
			tag_html += '<img src="'+me.imgTag.src+'" class="imgTag">';
			tag_html += '<div style="position: relative;">';
				tag_html += '<div class="tag_detail_wrapper" style="display:none;" id="img_tag_detail_'+tag.tag_id+"_"+tag.t0+"_"+tag.t1+'">';
					tag_html += '<div class="video_tag_detail"><div class="image_wrapper"><img src="'+tag.image+'" /></div><div class="item_info"><p class="item_store">'+tag.merchant+'</p><p class="item_name">'+tag.name+'</a><p class="item_price">'+tag.price+'</p><a class="buy_button" href="'+tag.url+'" target="_blank"></a></div></div>';
				tag_html += '</div>';
			tag_html += '</div>';
		tag_html += '</div>';

		var tag_div = document.createElement("div");
		tag_div.setAttribute("id", "img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1);
		tag_div.setAttribute("class", "img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1);
		tag_div.setAttribute("style", "position: absolute; top: "+(y-12)+"px; left: "+(x-12)+"px; width: 24px; height: 24px;")
		tag_div.innerHTML = tag_html;
		vp.myHtmlElem.appendChild(tag_div);

    }

    this.ctx.clearRect(0, 0, vp.width, vp.height);
    this.ctx.beginPath();

	var drawn_tags = new Array();
	
    for (var i = 0; i < this.video_tags.length; i++)
    {
        var tag = this.video_tags[i];

        var draw_tag = true;
        if (Number(currTime) < tag.t0) {
            //tag.detail_visibility = false;
            //$(".tag_div_"+tag.tag_id).remove();
            //continue;
            draw_tag = false;
        }
        if (Number(currTime) > tag.t1) {
            //tag.detail_visibility = false;
            //$(".tag_div_"+tag.tag_id).remove();
            //continue;
            draw_tag = false;
        }

            //console.log("---------->>> interval", currTime);
        //interval match
        if(draw_tag) { 

			/* actual video size may be different than canvas size (vertical videos for example) */
			var left_margin = (vp.width - vp.actualWidth)/2;
			var top_margin = (vp.height - vp.actualHeight)/2;

			var delta_x = (tag.x1 - tag.x0) * (currTime - tag.t0) / (tag.t1 - tag.t0) + left_margin;
			var delta_y = (tag.y1 - tag.y0) * (currTime - tag.t0) / (tag.t1 - tag.t0) + top_margin;

			if(document.getElementById("img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1)) {
				// move
				document.getElementById("img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1).style.display = "";
				move(tag.x0*vp.width + delta_x, tag.y0*vp.height + delta_y, tag);
			} else {
				
				draw(tag.x0*vp.width + delta_x, tag.y0*vp.height + delta_y, tag);
				
				//$("#debug").append("create: "+ "img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1+"<br>")
			}
        } else {
			if(document.getElementById("img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1)) {
				document.getElementById("img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1).remove();
				//$("#debug").append("remove => #img_tag_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1+"<br>")
			}
			
//			element.parentNode.removeChild(element);
			
            //$("#tag_div_"+tag.tag_id+"_"+tag.t0+"_"+tag.t1).remove();
        }

    } //end for i   



    //re-trigger
    requestAnimFrame(function() {
        me.drawTags(vp)
    });
	

}


VideoTagPluggin.prototype.reloadMetadata = function(vp)
{
    console.log("VideoTagPluggin.reloadMetadata");

    this.video_tags = [];
}

VideoTagPluggin.prototype.saveSnapshot = function(vp)
{
    console.log("VideoTagPluggin.saveSnapshot");

    this.snapshot = this.video_tags;
}

VideoTagPluggin.prototype.restoreSnapshot = function(vp)
{
    console.log("VideoTagPluggin.restoreSnapshot");

    if (this.snapshot != null) {
        this.video_tags = this.snapshot;
    }
}
