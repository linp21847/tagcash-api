$(document).ready(function(){
	$(".toolTip").tipTip({
		maxWidth: "auto", 
		defaultPosition: "top",
		delay: 200,
		edgeOffset: 10
		});
})

function popupAlert(type, text){
	var elem = $('#popup-alert');
	elem.removeClass();
	elem.addClass('popup-' + type);
	elem.find('#content').html(text);
	elem.fadeIn(300).delay(4000).fadeOut(300);
}

function closePopup(){
	var elem = $('#close_popup');
	elem.parent().hide();
	return false;
}

function loadError(selector, value) {
	var html = selector.parent().find('.frm_error').html();
	selector.parent().find('.frm_error').html(value).fadeIn();
	
}

/*========================================================
 * Fancy Box
 * elem => selector
 *========================================================*/
function fancyBox(elem, scroll){
	if(scroll == undefined){
		scroll = 'none';
	}
	$('.fancyboxGB').show();
	elem 		= $(elem);
	if (!elem.data("fancybox")) {
		elem.data("fancybox", true);
		elem.fancybox({
			'titleShow' 	: false,
			'transitionIn'	: 'fade',
			'transitionOut'	: 'fade',
			'centerOnScroll': true,
			'scrolling'		: scroll,
//			'hideOnOverlayClick':false,
//			'hideOnContentClick':false,
			onComplete		: function(data){
				
			},
			onClosed		: function(data){
				$('.fancyboxGB').hide();
			}
		}).trigger('click');
	}
	return false; 
}

/*=========================================================
 * Place holder
 * elem => selector
 * value => String what you want to show
 *=========================================================*/
function placeholder(elem, value){
	if(elem.val() == ''){
		elem.addClass('placeholder');
		elem.val(value);
		elem.focus(function(){
			if($(this).val() == '' || $(this).hasClass('placeholder')){
				$(this).val('');
				$(this).removeClass('placeholder');
			}
		});
		elem.blur(function(){
			if($(this).val() == ''){
				$(this).addClass('placeholder');
				$(this).val(value);
			}
		});
	}
}

function is_float(mixed_var) {
  return +mixed_var === mixed_var && (!isFinite(mixed_var) || !!(mixed_var % 1));
}

function is_int(mixed_var) {
	 return mixed_var === +mixed_var && isFinite(mixed_var) && !(mixed_var % 1);
}

