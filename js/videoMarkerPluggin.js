
function VideoMarkerPluggin(vs_main, video_markers_json)
{
  console.log("VideoMarkerPluggin.constructor");
  this.debug = true;
  var me = this;
  this.video_markers = video_markers_json;
  this.video_main = vs_main;
  this.video_duration = null;
  
  $("#video"+vs_main.id).on("loadedmetadata", function()
  {
     if(me.debug) 
        console.log("METADATA LOADED VideoMarkerPluggin !!!!!!   duration: "  + vs_main.api_getVideoDuration());
     me.video_duration = vs_main.api_getVideoDuration();   
  });
  this.slider_length = 288; //px
  this.start_offset = 64;//px from the start of the player until the seeker
}


VideoMarkerPluggin.prototype.createHtml = function(vp) 
{
  console.log("VideoMarkerPluggin.createHtml");
  var me = this; 
  me.video_duration = vp.api_getVideoDuration();
  console.log("video_duration=" +  me.video_duration);
}

VideoMarkerPluggin.prototype.hookIt = function(vp) 
{
  console.log("VideoMarkerPluggin.hookIt");
  
  var me = this;      
  vp.video_el.addEventListener("timeupdate", function() {me.timeupdateMarkers(vp)}, false);
}

VideoMarkerPluggin.prototype.saveSnapshot = function(vp) 
{
  console.log("VideoMarkerPluggin.saveSnapshot");
}  

VideoMarkerPluggin.prototype.restoreSnapshot = function(vp) 
{
  console.log("VideoMarkerPluggin.restoreSnapshot");
}  


VideoMarkerPluggin.prototype.reloadMetadata = function(vp) 
{
  console.log("VideoMarkerPluggin.reloadMetadata");
}  

VideoMarkerPluggin.prototype.timeupdateMarkers = function(vp) 
{
  console.log("VideoMarkerPluggin.timeupdateMarkers vp.id="+vp.id);
  var seekslider = document.getElementById("seekslider" + vp.id);
  var currTime = vp.api_getCurrentTime();
  var background_image = seekslider.style.backgroundImage;
  //console.log("background_image"); console.log(background_image);
  
  var background_position = seekslider.style.backgroundPosition;
  //console.log("background_position"); console.log(background_position);
  
  var background_size = seekslider.style.backgroundSize;
  //console.log("background_size"); console.log(background_size);
    
  var me = this;
  
  if((me.video_markers["marker"] != null) && (me.video_markers["marker"].length > 0))
  { 
	  for(var l=0; l< me.video_markers["marker"].length; l++)
	  {	
          //console.log(me.video_markers["marker"][l].marker_start);          
          //console.log(me.video_markers["marker"][l].marker_duration);
          //console.log(currTime);
          var pos = Math.floor((me.slider_length * me.video_markers["marker"][l].marker_start) / me.video_duration);
          var size = Math.floor((me.slider_length * me.video_markers["marker"][l].marker_duration) / me.video_duration);
          
          background_image = "url(img/marker.png), " + background_image;
          //background_position = "50px 0px, " + background_position ;
          //background_size = "20px 7px, " + background_size;
          
          background_position = pos +"px 0px, " + background_position ;
          background_size = size +"px 7px, " + background_size;
          
          //show marker div
          if(
              (Number(currTime) > Number(me.video_markers["marker"][l].marker_start)) && 
              (Number(currTime) < Number(me.video_markers["marker"][l].marker_start) + Number(me.video_markers["marker"][l].marker_duration))
            )
          {
              var link = "";
              if(me.video_markers["marker"][l].marker_type == "link")
               link = '<a href="' + me.video_markers["marker"][l].marker_text +'" ><div style="color:white;  text-decoration: none; background-color: none;">'+ me.video_markers["marker"][l].marker_text +'</div></a>';
              else
               link = '<div style="color:white;  text-decoration: none; background-color: none;">'+ me.video_markers["marker"][l].marker_text +'</div>';
              var vpb = vp.id;              

 //             $("#" + vpb +"").find("#seekslider" + vp.id).after('<div id="marker" style="position: absolute; height:30px;' +                                
 //                                                               ' top: -35px; display: inline-block; background-color: #444444; border : 2px solid #1b1b1b; ' + 
 //                                                               ' z-index: 100;        -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)"; filter: alpha(opacity=50); opacity:.5;'+
 //                                                               ' "> '+link+'</div>');

              $("#" + vpb +"").find("#marker_" + l + "_"+ vp.id).remove();
              $("#" + vpb +"").find("#seekslider" + vp.id).after('<div id="marker_' + l + "_"+ vp.id + '" class="bubble">'+link+'</div>');
              $("#" + vpb +"").find("#marker_" + l + "_"+ vp.id).css("left", Number(pos   + me.start_offset ) +"px");
          }
          else
          {
			 //console.log("hide marker !!!!!!!!!!!!!!!  " + "#marker_" + l + "_"+ vp.id);
			 $("#marker_" + l + "_"+ vp.id).empty().remove();
		  }
         
      }
  }
  seekslider.setAttribute("style", 
                       "background-color: #000000; " + 
                       "background-image: " + background_image + "; " +
                       "background-position: " + background_position +  "; " +
                       "background-repeat: no-repeat; " + "; " +
                       "background-size: " + background_size + "; "
                      );   
  
}

